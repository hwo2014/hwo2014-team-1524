var _ = require("underscore");
var raceData = require("./racedata.js");
var logger = require("./logger.js");

console.log("GameState init");

var state = {
  myColor: null,
  telemetries: {},
  turbo: null,
  lastGameTick: null,
  lastThrottle: 0,
  crashLog: {} // {carColor: [{type: "crash", gameTick: 42}, {type: "spawn", gameTick: 84}]}
};

function Telemetry(gameTick, throttle, pos, speed, acceleration){
  this.gameTick = gameTick;
  this.prevThrottle = throttle;
  this.pos = pos;
  this.speed = speed;
  this.acceleration = acceleration;
};

function calcTelemetry(pos, lastTelemetry, gameTick){
  var speed = 0,
      acceleration = 0;
  
  if (lastTelemetry && lastTelemetry.gameTick){
    var tickDelta = gameTick - lastTelemetry.gameTick;
    speed = raceData.distanceBetween(lastTelemetry.pos, pos) / tickDelta; // speed = distance per tick
    acceleration = (speed - lastTelemetry.speed) / tickDelta;    
  }
  return new Telemetry(gameTick, state.lastThrottle, pos, speed, acceleration);
}


exports.updateTelemetries = function(newPositions, gameTick){
  _.each(newPositions, function(pos){
    var color = pos.id.color;
    if (!_.has(state.telemetries, color)){
      state.telemetries[color] = [];
    }
    var telemetry = calcTelemetry(pos, _.last(state.telemetries[color]), gameTick);
    state.telemetries[color].push(telemetry);
  });
  var myTelemetry = _.last(state.telemetries[state.myColor]);
//  console.log("updateTelemetries: ", myTelemetry);
  logger.logTelemetry(myTelemetry);
  state.lastGameTick = gameTick;
};

exports.setMyColor = function(color){      
  console.log("My color is", color);
  state.myColor = color;
};

exports.myColor = function(){      
  return state.myColor;
};

exports.setTurbo = function(turbo){
  state.turbo = turbo;
};

exports.getTurbo = function(){
  return state.turbo;
};

exports.getLastGameTick = function(){
  return state.lastGameTick;
};

exports.getTelemetries = function(){
  return state.telemetries;
};

exports.getCarTelemetry = function(carColor){
  return state.telemetries[carColor];
};

exports.getMyLastTelemetry = function(){
  return _.last(state.telemetries[state.myColor]);
};

exports.setThrottle = function(throttle){
  state.lastThrottle = throttle;
}

exports.handleCrash = function(carColor, gameTick){
  if (!_.has(state.crashLog, carColor)){
    state.crashLog[carColor] = [];
  }     
  state.crashLog[carColor].push({type: "crash", gameTick: gameTick});
  console.log("%s car crashed at gametick %s", carColor, gameTick);
};

exports.handleSpawn = function(carColor, gameTick){
  state.crashLog[carColor].push({type: "spawn", gameTick: gameTick});
  console.log("%s car spawned at gametick %s", carColor, gameTick);
};

exports.isCrashed = function(carColor){
  var lastCrashLogEntry = _.last(state.crashLog[carColor]);
  return _.isObject(lastCrashLogEntry) && lastCrashLogEntry.type === "crash";
};

exports.wasCrashed = function(carColor, gameTick){
  var crashLog = _.toArray(state.crashLog[carColor]);
  var crashed = false;
  for (var i = 0; i < crashLog.length; i++){
    if (crashLog[i].gameTick > gameTick){
      break;
    }
    crashed = crashLog[i].type === "crash";
  }
  return crashed;
};

