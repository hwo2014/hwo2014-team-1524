var _ = require("underscore");

console.log("Racedata init");

var raceData = null;
var trackLength = 0;

function distanceBetween(pos1, pos2){
  var pieces = raceData.track.pieces;
  var i1 = pos1.piecePosition.pieceIndex,
      i2 = pos2.piecePosition.pieceIndex,
      lane = raceData.track.lanes[pos1.piecePosition.lane.startLaneIndex];
  var distance = 0;
  while (i1 != i2){
    distance += pieceLength(pieces[i1], lane);
    i1 = (i1 + 1) % pieces.length;
  }
  distance -= pos1.piecePosition.inPieceDistance;
  distance += pos2.piecePosition.inPieceDistance;
  return distance;
}

function pieceLength(piece, lane){
  if (_.has(piece, "length")){
    return piece.length;
  }
  var radius = piece.radius - (sign(piece.angle) * lane.distanceFromCenter);
  return arcLength(piece.angle, radius);
}

function arcLength(angle, radius){
  return Math.abs(angle / 360 * 2 * Math.PI * radius);
}

function nextCurvePieceIndex(pos){
  var pieces = raceData.track.pieces,
      index = (pos.piecePosition.pieceIndex + 1) % pieces.length;
  while (!_.has(pieces[index], "angle")){
    index = (index + 1) % pieces.length;
  }
  return index;
}

function distanceToNextCurve(pos){
  var curvePieceIndex = nextCurvePieceIndex(pos),
      curvePos = {piecePosition: {pieceIndex: curvePieceIndex, inPieceDistance: 0}};
  return distanceBetween(pos, curvePos);
};

function sign(val){
  if (isNaN(val))
    return NaN;
  else if (val > 0)
    return 1;
  else if (val < 0)
    return -1;
  else
    return 0;
}

exports.isCurve = function(pos){
  var piece = raceData.track.pieces[pos.piecePosition.pieceIndex];
  return !_.has(piece, "length");
};

exports.setRaceData = function(race){
  console.log("Got race data!");
  raceData = race;    
  var dummyLane = {distanceFromCenter: 0};
  _.each(raceData.track.pieces, function(piece){
    trackLength += pieceLength(piece, dummyLane);
  });
  console.log("The track is %s units long", trackLength);
};

exports.trackLength = function(){
  return trackLength;
};

exports.numTrackPieces = function(){
  return raceData.track.pieces.length;
};

exports.pieceLength = pieceLength;
exports.arcLength = arcLength;
exports.distanceBetween = distanceBetween;
exports.nextCurvePieceIndex = nextCurvePieceIndex;
exports.distanceToNextCurve = distanceToNextCurve;




