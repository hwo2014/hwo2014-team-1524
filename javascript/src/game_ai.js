var raceData = require("./racedata.js");
var gameState = require("./gamestate.js");
var _ = require("underscore");

console.log("Game AI init");

var testModeState = {
  accelerationTested: false,
  breakingTested: false
};

function testLimitsMode(){
  var telemetry = gameState.getMyLastTelemetry();
  if (telemetry.pos.piecePosition.pieceIndex >= raceData.numTrackPieces() - 2){
    console.log("End of limits test mode");
    exports.calculateReply = raceMode;
    return raceMode();
  }
  var myColor = gameState.myColor();
  var throttle = 1.0;

  if (!testModeState.accelerationTested){
    if (telemetry.acceleration === 0 || telemetry.acceleration > 0.1){
      throttle = 1.0;
    } else {
      console.log("Acceleration test complete");
      testModeState.accelerationTested = true;
      throttle = 0;
    }
  } else if (!testModeState.breakingTested){
    if (telemetry.speed > 0){
      throttle = 0;
    } else {
      console.log("Break test complete");
      testModeState.breakingTested = true;
      throttle = 1.0;
    }
  } else {
    // TODO: acceleration and brake test complete, now what?
    throttle = 0.5;
  }

  if (gameState.isCrashed(myColor)){
    throttle = 0;
  }

  return {
    msgType: "throttle",
    data: throttle
  };
}

function raceMode(){
  var myColor = gameState.myColor();
  var throttle = 0.6;
  if (gameState.isCrashed(myColor)){
    throttle = 0;
  }

  return {
    msgType: "throttle",
    data: throttle
  };
}



exports.calculateReply = testLimitsMode;





