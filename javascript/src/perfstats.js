var raceData = require("./racedata.js");
var gameState = require("./gamestate.js");
var _ = require("underscore");

console.log("perfstats init");

var maxSpeed = 0;
var accelerationStats = {};
var brakeStats = {};
var curveStats = {};

function updateStats(){
  _.each(gameState.getTelemetries(), function(telemetries, carColor){
    var telemetry = _.last(telemetries);
    if (telemetry){
      maxSpeed = Math.max(maxSpeed, telemetry.speed);
      updateCurveStats(carColor, telemetries);
    }
  });
}

function updateCurveStats(carColor, telemetries){
  var size = _.size(telemetries);
  if (size >= 2){
    var last = telemetries[size - 1],
        sndLast = telemetries[size - 2],
        pieceIndex = sndLast.pos.piecePosition.pieceIndex;
    
    if (raceData.isCurve(sndLast.pos) && last.pos.piecePosition.pieceIndex !== pieceIndex){
      // just came out of a curve
      
      var numTicks = 0;
      var avgSpeed = 0;
      var avgSlipAngle = 0;
      for (var i = size - 2; i >= 0; i--){
        var tm = telemetries[i];
        if (tm.pos.piecePosition.pieceIndex != pieceIndex){
          break;
        }        
        if (gameState.wasCrashed(carColor, tm.gameTick)){
          // If car crashed in this curve then don't record stats
          return;
        }
        numTicks++;
        avgSpeed += tm.speed;
        avgSlipAngle += Math.abs(tm.pos.angle);
      }
      if (numTicks > 0){
        avgSpeed = avgSpeed / numTicks;
        avgSlipAngle = avgSlipAngle / numTicks;
      }
      if (!_.has(curveStats, pieceIndex) || avgSpeed > curveStats[pieceIndex].avgSpeed){
        curveStats[pieceIndex] = {
          avgSpeed: avgSpeed,
          avgSlipAngle: avgSlipAngle
        };
        console.log("Curve stats updated: ", curveStats);
      }
       
    } 
  }
}

// return {avgSpeed: 42, avgSlipAngle: 22} or null
function getCurveStats(curvePieceIndex){
  if (!_.has(curveStats, curvePieceIndex)){
    return null;
  }
  return curveStats[curvePieceIndex];
}

module.exports = {
  updateStats: updateStats
};



