var fs = require("fs");

try {
  fs.mkdirSync("logs");
} catch (e){
}

var receivedStream = fs.createWriteStream("logs/received-" + Date.now() + ".json");
var telemetryStream = fs.createWriteStream("logs/telemetry-" + Date.now() + ".json");

exports.receivedMsg = function(msg){
  receivedStream.write(JSON.stringify(msg));
  receivedStream.write("\n");
};

exports.logTelemetry = function(telemetry){
  telemetryStream.write(JSON.stringify(telemetry));
  telemetryStream.write("\n");
};










