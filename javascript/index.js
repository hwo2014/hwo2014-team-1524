var net = require("net");
var JSONStream = require('JSONStream');
var raceData = require("./src/racedata.js");
var gameState = require("./src/gamestate.js");
var logger = require("./src/logger.js");
var gameAI = require("./src/game_ai.js");
var perfStats = require("./src/perfstats.js");

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return sendResponse({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});


function sendResponse(response) {
  if (response.msgType === "throttle"){
    gameState.setThrottle(response.data);
  }
  client.write(JSON.stringify(response));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(msg) {
  logger.receivedMsg(msg);

  if (msg.msgType === 'carPositions') {
    gameState.updateTelemetries(msg.data, msg.gameTick);
    perfStats.updateStats();
    sendResponse(gameAI.calculateReply());
  } else if (msg.msgType === "gameStart"){
    console.log('Race started');
    sendResponse({
      msgType: "throttle",
      data: 1.0
    });    
  } else {
    if (msg.msgType === 'join') {
      console.log('Joined');
    } else if (msg.msgType === "gameInit"){
      raceData.setRaceData(msg.data.race);
    } else if (msg.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (msg.msgType === "yourCar") {
      gameState.setMyColor(msg.data.color);
    } else if (msg.msgType === "turboAvailable"){
      gameState.setTurbo(msg.data);
    } else if (msg.msgType === "crash"){
      gameState.handleCrash(msg.data.color, msg.gameTick);
    } else if (msg.msgType === "spawn"){
      gameState.handleSpawn(msg.data.color, msg.gameTick);
    } else {
      console.log("Unhandled message", msg);
    }

    sendResponse({
      msgType: "ping",
      data: {}
    });
  }
  
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
