﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TheAITeam.Forms;

namespace Visualizer.Forms
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            string args = "join " +
                          cmbTrack.SelectedItem.ToString() + " " +
                          int.Parse(cmbPlayerCount.SelectedItem.ToString().First().ToString()) + " " +
                          cmbBotName.SelectedItem.ToString() + " " +
                          txtPassword.Text;
            Process.Start("Visualizer.exe", args);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process.Start("Visualizer.exe", "replay");
        }
    }
}
