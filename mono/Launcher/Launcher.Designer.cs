﻿namespace Visualizer.Forms
{
    partial class Launcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTrack = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPlayerCount = new System.Windows.Forms.ComboBox();
            this.btnJoin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbBotName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Replay last:";
            // 
            // cmbTrack
            // 
            this.cmbTrack.FormattingEnabled = true;
            this.cmbTrack.Items.AddRange(new object[] {
            "keimola",
            "germany",
            "usa",
            "france"});
            this.cmbTrack.Location = new System.Drawing.Point(74, 10);
            this.cmbTrack.Name = "cmbTrack";
            this.cmbTrack.Size = new System.Drawing.Size(121, 21);
            this.cmbTrack.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "New race:";
            // 
            // cmbPlayerCount
            // 
            this.cmbPlayerCount.FormattingEnabled = true;
            this.cmbPlayerCount.Items.AddRange(new object[] {
            "1 player",
            "2 players",
            "3 players",
            "4 players"});
            this.cmbPlayerCount.Location = new System.Drawing.Point(201, 10);
            this.cmbPlayerCount.Name = "cmbPlayerCount";
            this.cmbPlayerCount.Size = new System.Drawing.Size(121, 21);
            this.cmbPlayerCount.TabIndex = 11;
            // 
            // btnJoin
            // 
            this.btnJoin.Location = new System.Drawing.Point(653, 10);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(75, 23);
            this.btnJoin.TabIndex = 13;
            this.btnJoin.Text = "Join";
            this.btnJoin.UseVisualStyleBackColor = true;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(389, 10);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(100, 20);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.Text = "p4ssw0rd";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "password:";
            // 
            // cmbBotName
            // 
            this.cmbBotName.FormattingEnabled = true;
            this.cmbBotName.Items.AddRange(new object[] {
            "Slippy",
            "Speedy",
            "Granny",
            "SimpleAI",
            "Crashy"});
            this.cmbBotName.Location = new System.Drawing.Point(526, 11);
            this.cmbBotName.Name = "cmbBotName";
            this.cmbBotName.Size = new System.Drawing.Size(121, 21);
            this.cmbBotName.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(495, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "bot:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(74, 40);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 19;
            this.button3.Text = "Launch";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 78);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbTrack);
            this.Controls.Add(this.cmbBotName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbPlayerCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnJoin);
            this.Name = "Launcher";
            this.Text = "Launcher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cmbTrack;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cmbPlayerCount;
        public System.Windows.Forms.Button btnJoin;
        public System.Windows.Forms.TextBox txtPassword;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbBotName;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button button3;
    }
}