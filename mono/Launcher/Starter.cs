﻿using System;

namespace Visualizer.Forms
{
    class Starter
    {
        private Launcher launcher = new Launcher();

        [STAThread]
        static void Main(string[] args)
        {
            var starter = new Starter();
            System.Windows.Forms.Application.EnableVisualStyles();
        }

        public static void VisualizerFormUnload(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            //System.Windows.Forms.Application.Exit();
            //Environment.Exit(0);
        }

        public Starter()
        {
            launcher.FormClosed += VisualizerFormUnload;

            launcher.cmbBotName.SelectedItem = "Speedy";
            launcher.cmbPlayerCount.SelectedItem = "1 player";
            launcher.cmbTrack.SelectedItem = "keimola";

            launcher.ShowDialog();
        }
    }
}
