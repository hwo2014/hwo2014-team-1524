﻿using TheAITeam.Classes;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Logic;
using TheAITeam.Classes.Messaging;
using System.Linq;

public class DummyVisualizer : IVisualizer
{
    public void DoEvents()
    {
        ;
    }

    public void VisualizeRaceState(GameState state, Messages.SendMsg throttle)
    {
        ;
    }

    public void Init()
    {
        ;
    }

    public void InitGraphics(GameState state)
    {
        ;
    }

    public bool IsAlive()
    {
        return true;
    }
}
class Program
{
    public static void Main(string[] args)
    {
        string host = Bot.DEFAULT_HOST;
        int port = Bot.DEFAULT_PORT;

        string botName = Bot.DEFAULT_BOTNAME;
        string botKey = Bot.DEFAULT_BOTKEY;
        bool logTofile = true;

        if (args.Length > 3)
        {
            host = args[0];
            port = int.Parse(args[1]);
            botName = args[2];
            botKey = args[3];
            logTofile = false;
        }
        //Logger.Write(Logger.Type.Info, "Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        var replay = args.Contains("replay");

        AI ai = new Speedy(); //This one will be selected in the actual races!!!
        var joinMsg = new Messages.Join(botName, botKey);

        Bot.Start(replay, logTofile, host, port, ai, new DummyVisualizer(), joinMsg);
    }
}
