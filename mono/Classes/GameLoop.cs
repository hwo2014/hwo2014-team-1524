﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Logic;
using TheAITeam.Classes.Messaging;

namespace TheAITeam.Classes
{
    public interface IVisualizer
    {
        void DoEvents();
        void VisualizeRaceState(GameState state, Messages.SendMsg msg);
        void Init();

        void InitGraphics(GameState state);
        bool IsAlive();
    }

    class GameLoop
    {
        public IVisualizer visualizer;

        private GameState gameState = null;

        private System.IO.StreamReader readFromServer;
        private System.IO.StreamWriter sendToServer;

        private AI ai;

        public GameLoop(AI ai, System.IO.StreamReader reader, System.IO.StreamWriter writer, IVisualizer visualizer)
        {
            visualizer.Init();

            gameState = new GameState();

            this.ai = ai;
            this.ai.SetGameState(gameState);

            this.readFromServer = reader;
            this.sendToServer = writer;
            this.visualizer = visualizer;
        }

        public void Start(Messages.SendMsg joinMessage)
        {
            SendMessage(joinMessage);

            string line = "";
            while((line = readFromServer.ReadLine()) != null)
            {
                if (!visualizer.IsAlive())
                    break;

                visualizer.DoEvents();

                var receivedMessage = JsonConvert.DeserializeObject<MsgWrapperWithTick>(line);
                Logger.Write(Logger.Type.Receive, line);

                switch(receivedMessage.msgType)
                {
                    case "carPositions":
                        List<CarPosition> carPositions = JsonConvert.DeserializeObject<List<CarPosition>>(receivedMessage.data.ToString());
                        gameState.UpdateCarPosition(carPositions, receivedMessage.gameTick);
                        break;
                    case "join":
                    case "joinRace":
                        break;
                    case "gameInit":
                        Game game = JsonConvert.DeserializeObject<Game>(receivedMessage.data.ToString());
                        gameState.Initialize(game);
                        visualizer.InitGraphics(gameState);
                        Logger.Write(Logger.Type.Info, "game initialized.");
                        break;
                    case "gameEnd":
                        ai.GameFinished();
                        break;
                    case "gameStart":
                        gameState.StartRace();
                        break;
                    case "yourCar":
                        CarID myCar = JsonConvert.DeserializeObject<CarID>(receivedMessage.data.ToString());
                        gameState.AssignMyCar(myCar);
                        break;
                    case "tournamentEnd":
                        break;
                    case "crash":
                        Logger.Write(Logger.Type.Info, receivedMessage.data.ToString());
                        var crashedCar = JsonConvert.DeserializeObject<CarID>(receivedMessage.data.ToString());

                        if (crashedCar.name == gameState.Me.id.name)
                        {
                            ai.Crashed();
                        }
                        gameState.Crashed(crashedCar, receivedMessage.gameTick);
                        break;
                    case "spawn":
                        var car = JsonConvert.DeserializeObject<CarID>(receivedMessage.data.ToString());
                        gameState.Spawn(car, receivedMessage.gameTick);
                        break;
                    case "lapFinished":
                        var lapFinished = JsonConvert.DeserializeObject<LapFinished>(receivedMessage.data.ToString());
                        Logger.Write(Logger.Type.Info, "Lap finished ticks " + lapFinished.lapTime.ticks + " millis " + lapFinished.lapTime.millis);
                        break;
                    case "dnf":
                        Logger.Write(Logger.Type.Info, receivedMessage.data.ToString());
                        break;
                    case "finish":
                        Logger.Write(Logger.Type.Info, receivedMessage.data.ToString());
                        break;
                    case "turboAvailable":
                        var turboAvailable = JsonConvert.DeserializeObject<TurboAvailable>(receivedMessage.data.ToString());
                        if (gameState.Me.CurrentState.crashed)
                        {
                            Logger.Write(Logger.Type.Info, "turbo would have been given, but car was crashed");
                        }
                        else
                        {
                            Logger.Write(Logger.Type.Info, "turbo available");
                            gameState.NewTurboAvailable(turboAvailable);
                        }
                        break;
                    case "turboStart":
                        Logger.Write(Logger.Type.Info, receivedMessage.data.ToString());
                        var turboStart = JsonConvert.DeserializeObject<CarID>(receivedMessage.data.ToString());
                        gameState.TurboStart(turboStart, receivedMessage.gameTick);
                        break;
                    case "turboEnd":
                        Logger.Write(Logger.Type.Info, receivedMessage.data.ToString());
                        var turboEnd = JsonConvert.DeserializeObject<CarID>(receivedMessage.data.ToString());
                        gameState.TurboEnd(turboEnd, receivedMessage.gameTick);
                        break;
                    /*{"msgType":"turboStart","data":{"name":"Rosberg","color":"blue"}, "gameTick": 124}
                      {"msgType":"turboEnd","data":{"name":"Rosberg","color":"blue"}, "gameTick": 154}*/
                    default:
                        Logger.Write(Logger.Type.Info, "other message: " + receivedMessage.msgType);
                        break;
                }

                if (gameState.RaceInProgress())
                {
                    var reply = ai.CalculateReply();

                    SendMessage(reply, receivedMessage.gameTick);

                    if(reply as Messages.Throttle != null)
                        gameState.UpdateThrottleInTelemetry(((Messages.Throttle)reply).value);

                    visualizer.VisualizeRaceState(gameState, reply);
                }
                else
                {
                    SendMessage(new Messages.Ping());
                }
            }
        }

        private void SendMessage(Messages.SendMsg messageToSend)
        {
            SendMessage(messageToSend, 0);
        }

        private void SendMessage(Messages.SendMsg messageToSend, int gameTick)
        {
            var json = messageToSend.ToJson(gameTick);
            Logger.Write(Logger.Type.Send, json);
            sendToServer.WriteLine(json);
        }
    }
}
