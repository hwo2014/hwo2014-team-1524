﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
using Newtonsoft.Json;


public static class Logger
{
    public enum Type
    {
        Send,
        Receive,
        AIMessage,
        Info
    }

    public interface IScreenWriter
    {
        void WriteLine(string s);
    }

    public class WriteToConsole : IScreenWriter
    {
        public void WriteLine(string s)
        {
            Console.WriteLine(s);
        }
    }

    private static IScreenWriter screenWriter = new WriteToConsole();

    private static StreamWriter writer;
    private static List<Type> logToFile;
    private static List<Type> logToScreen; 

    public static void LogToFile(List<Type> types)
    {
        logToFile = types;

        if (logToFile.Count != 0)
        {
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");
            var path = "logs" + Path.DirectorySeparatorChar + DateTime.Now.ToString("o").Split('+')[0].Replace(":", ".") + ".txt";
            writer = new StreamWriter(path);
            writer.AutoFlush = true;
        }
    }

    public static void LogToScreen(List<Type> types)
    {
        logToScreen = types;
    }

    public class LogEvent
    {
        public LogEvent(Type type, DateTime dt, string data)
        {
            this.type = type.ToString();
            this.time = dt.ToString("o");
            this.data = data;
        }
        public string type;
        public string time;
        public string data;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public static void Write(Type type, string s)
    {
        var logEvent = (new LogEvent(type, DateTime.Now, s).ToJson());
        if (logToFile.Contains(type))
            writer.WriteLine(logEvent);
        if(logToScreen.Contains(type))
            screenWriter.WriteLine(logEvent);
    }

    public static void SetLogToScreenWriter(IScreenWriter writer)
    {
        screenWriter = writer;
    }
}

/// <summary>
/// Reads logfiles, skips everything execpt receive data - events, and returns the logged received data with a simulated delay;
/// </summary>
class LogReader : StreamReader
{
    DateTime previous = DateTime.MinValue;

    public LogReader(Stream stream) : base(stream)
    {
        
    }

    public override string ReadLine()
    {
        Logger.LogEvent logEvent = null;
             
        while(logEvent == null || !logEvent.type.Equals(Logger.Type.Receive.ToString()))
        {
            string s = base.ReadLine();
            if (s == null)
                return null;
            logEvent = JsonConvert.DeserializeObject<Logger.LogEvent>(s);
        }

        var thisTime = DateTime.Parse(logEvent.time);
        var lag = previous == DateTime.MinValue ? TimeSpan.Zero : thisTime - previous;

        System.Threading.Thread.Sleep(lag);
        string d = logEvent.data;
        return d;
    }
}

class DummyWriter : StreamWriter
{
    public DummyWriter(Stream stream) : base(stream)
    {
    }

    public override void WriteLine(string s)
    {
        ;
    }
}