﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAITeam.Classes.Tools
{
    public static class Curve
    {
        public static double ArcLength(double radius, double angle)
        {
            return 2.0*Math.PI*radius*(Math.Abs(angle)/360.0);
        }

        public static double ArcCurvature(double radius, double angle)
        {
            return angle / ArcLength(radius, angle); //TODO: insert real formula here
        }
    }
}
