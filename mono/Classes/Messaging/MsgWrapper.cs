using System;

public class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }

    public enum MessageTypes
    {
        Join,
        Ping,
    }

    /* SERVER SETUP
     * "createRace"
     */

    /* BOT COMMANDS
     * "join"
     * "throttle"
     * "switchLane"
     * "joinRace"
     * "ping"
     */

    /* SERVER COMMANDS
     * "yourCar"
     * "gameInit"
     * "gameStart"
     * "gameEnd"
     * "carPositions"
     * "tournamentEnd"
     * "crash"
     * "spawn"
     * "lapFinished"
     * "dnf"
     * "finish"
     */
}

public class MsgWrapperWithTick : MsgWrapper
{
    public int gameTick;

    public MsgWrapperWithTick(string msgType, Object data, int gameTick)
        : base(msgType, data)
    {
        this.gameTick = gameTick;
    }
}
