﻿using System;
using Newtonsoft.Json;

namespace TheAITeam.Classes.Messaging
{
    public class Messages
    {
        public abstract class SendMsg
        {
            public string ToJson(int gameTick)
            {
                if(gameTick == 0)
                    return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
                else
                    return JsonConvert.SerializeObject(new MsgWrapperWithTick(this.MsgType(), this.MsgData(), gameTick));
            }

            protected virtual Object MsgData()
            {
                return this;
            }

            protected abstract string MsgType();
        }

        public class Join : SendMsg
        {
            public string name;
            public string key;

            public Join(string name, string key)
            {
                this.name = name;
                this.key = key;
            }

            protected override string MsgType()
            {
                return "join";
            }
        }

        public class Ping : SendMsg
        {
            protected override string MsgType()
            {
                return "ping";
            }
        }

        public class SwitchLanes : SendMsg
        {
            public string value;

            // Direction can be either "Left" or "Right"
            public SwitchLanes(string Direction)
            {
                this.value = Direction;
            }

            protected override Object MsgData()
            {
                return this.value;
            }

            protected override string MsgType()
            {
                return "switchLane";
            }
        }

        public class Throttle : SendMsg
        {
            public double value;

            public Throttle(double value)
            {
                this.value = value;
            }

            protected override Object MsgData()
            {
                return this.value;
            }

            protected override string MsgType()
            {
                return "throttle";
            }
        }

        public class Turbo : SendMsg
        {
            public string message;

            public Turbo(string message)
            {
                this.message = message;
            }

            protected override string MsgType()
            {
                return "turbo";
            }

            protected override object MsgData()
            {
                return this.message;
            }
        }

        public class BotId
        {
            public string name;
            public string key;

            public BotId(string botName, string botKey)
            {
                this.name = botName;
                this.key = botKey;
            }
        }

        public class JoinRace : SendMsg
        {
            public class BotID
            {
                public string name;
                public string key;
            }

            protected override string MsgType()
            {
                return "joinRace";
            }

            public BotID botId;
            public string trackName;
            public int carCount;
        }

        public class JoinRaceWithPassword : JoinRace
        {
            public string password;
        }

        public class CreateRace : JoinRaceWithPassword
        {
            protected override string MsgType()
            {
                return "createRace";
            }
        }
    }
}
