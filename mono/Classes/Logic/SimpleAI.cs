﻿
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Messaging;

namespace TheAITeam.Classes.Logic
{
    public class SimpleAI : AI
    {
        public override Messages.SendMsg CalculateReply()
        {
            double throttle = CalculateThrottleByPosition(gameState.Game.race.track);
            return new Messages.Throttle(throttle);
        }

        private double CalculateThrottleByPosition(Track track)
        {
            var carPosition = gameState.Me.CurrentState.CarPosition;

            // Make advanced logic in here, for now just check if we coming to bend decrease speed, if we on straight floor it
            if(carPosition == null || carPosition.piecePosition == null)
                return 1.0;

            if (IsInCurve(carPosition.piecePosition.pieceIndex) || IsCurveNext(carPosition.piecePosition.pieceIndex))
                return CalculateCurveSpeed(carPosition.piecePosition.pieceIndex);

            return 1.0;
        }

        private double CalculateCurveSpeed(int trackPieceId)
        {
            var carPosition = gameState.Me.CurrentState.CarPosition;

            int UpComingCurveDifficulty = CalculateUpComingCurveDifficulty(trackPieceId);
            if(!IsInCurve(trackPieceId))
                UpComingCurveDifficulty--; // Decrease severity if we aren't at the curve yet

            if(carPosition.angle > 5.0)
                UpComingCurveDifficulty += 2; // Increase severity if car is slipping
            else if(carPosition.angle > 15.0)
                UpComingCurveDifficulty += 4; // Increase severity if car is slipping

            switch(UpComingCurveDifficulty)
            {
                case 1:
                case 2:
                    return 1.0;
                case 3:
                    return 0.9;
                case 4:
                    return 0.7;
                case 5:
                case 6:
                    return 0.6;
                case 7:
                case 8:
                    return 0.5;
                case 9:
                    return 0.3;
                case 10:
                    return 0.2;
                case 11:
                case 12:
                    return 0.1;
                default:
                    return 0.5;
            }
        }

        private int CalculateUpComingCurveDifficulty(int trackPieceId)
        {
            Piece CurveTrackPiece = gameState.Game.race.track.GetNextCurveTrackPiece(trackPieceId);
            if(CurveTrackPiece != null)
            {
                int difficulty = 360 - (int) (360/CurveTrackPiece.angle);
                return difficulty / 36;
            }
            return 1;
        }

        private bool IsCurveNext(int trackPieceId)
        {
            Piece TrackPiece = gameState.Game.race.track.GetTrackPiece(trackPieceId + 1);
            if(TrackPiece != null)
            {
                return TrackPiece.IsCurve();
            }
            return false;
        }

        private bool IsInCurve(int trackPieceId)
        {
            Piece TrackPiece = gameState.Game.race.track.GetTrackPiece(trackPieceId);
            if(TrackPiece != null)
            {
                return TrackPiece.IsCurve();
            }
            return false;
        }
    }
}
