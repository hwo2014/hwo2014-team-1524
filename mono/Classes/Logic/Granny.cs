﻿using System;
using TheAITeam.Classes.Messaging;
using TheAITeam.Classes.Tools;

namespace TheAITeam.Classes.Logic
{
    public class Granny : AI
    {
        public override Messages.SendMsg CalculateReply()
        {
            var position = gameState.Me.CurrentState.CarPosition;
            var piece = gameState.Game.race.track.GetTrackPiece(position.piecePosition.pieceIndex);

            if(BreakForCurve(position, piece, FRICTION))
                return new Messages.Throttle(0);
            if (IsThroughApex(position, piece))
                return CalculateThrottle(CalculateTargetSpeed(gameState.Game.race.track.NextPiece(piece)), gameState.Me.CurrentState.speed);
            return CalculateThrottle(CalculateTargetSpeed(piece), gameState.Me.CurrentState.speed);
        }

        private Messages.Throttle CalculateThrottle(double targetSpeed, double currentSpeed)
        {
            var diff = targetSpeed - currentSpeed;
            if (Math.Abs(diff) < 0.02)
                return new Messages.Throttle(0.3);

            return new Messages.Throttle(currentSpeed > targetSpeed ? 0 : 1);
        }

        private bool IsThroughApex(CarPosition position, Piece piece)
        {
            //TODO: should precalculate all this stuff about lanes, track lengths, etc. when the race starts.
            var laneOffset = gameState.Game.race.track.lanes.Find(l => l.index == position.piecePosition.lane.endLaneIndex).distanceFromCenter;
            var result = position.piecePosition.inPieceDistance > piece.GetTrackLength(laneOffset);
            return result;
        }

    }
}
