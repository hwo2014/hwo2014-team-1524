﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Bson;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Messaging;

namespace TheAITeam.Classes.Logic
{
    public class Slippy : AI
    {
        protected static readonly int TARGET_SLIP = 25;
        protected static readonly double LONG_STRAIGHT_LENGTH = 400;

        private double lastSlip = double.MaxValue;

        public override Messages.SendMsg CalculateReply()
        {
            var position = gameState.Me.CurrentState.CarPosition;
            var piece = gameState.Game.race.track.GetTrackPiece(position.piecePosition.pieceIndex);
            var nextPiece = gameState.Game.race.track.NextPiece(piece);

            Messages.SendMsg result = null;

            var slip = Math.Abs(position.angle);
            string switchDirection;

            if (CalculateUpcomingStraightLength(piece, position) >= LONG_STRAIGHT_LENGTH && TurboAvailable())
                result = UseTurbo();
            else if (nextPiece.isSwitch && SwitchLanes(piece, position, out switchDirection))
            {
                Logger.Write(Logger.Type.AIMessage, "switching lanes");
                result = new Messages.SwitchLanes(switchDirection);
            }
            else if (BreakForCurve(position, piece, 0.1))
                result = new Messages.Throttle(0);
            else if (slip - lastSlip > 0.1) // slip increasing too fast, slow down.
                result = new Messages.Throttle(0);
            else
                result = new Messages.Throttle(slip > TARGET_SLIP ? 0 : 1);

            lastSlip = slip;

            return result;
        }
    }
}
