﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace TheAITeam.Classes.Data
{
    public class Telemetry
    {
        public Telemetry(CarID carId)
        {
            this.carId = carId;
        }

        public Telemetry(Telemetry tel)
        {
            this.carId = tel.carId;

            this.speed = tel.speed;
            this.acceleration = tel.acceleration;
            this.angle = tel.angle;
            this.throttle = tel.throttle;
            this.tick = tel.tick;
            this.wallTime = tel.wallTime;
            this.piecePosition = tel.piecePosition;
            this.crashed = tel.crashed;
            this.spawned = tel.spawned;
            this.turboActive = tel.turboActive;
        }

        private CarID carId;

        public double speed;
        public double acceleration;
        public double angle;
        public double throttle;
        public int tick;
        public DateTime wallTime;
        public PiecePosition piecePosition;
        public bool crashed;
        public bool spawned;
        public bool turboActive;

        public CarPosition CarPosition
        {
            get { return new CarPosition {angle = angle, piecePosition = piecePosition, id = carId}; }
        }
    }

    public class Player
    {
        public List<Telemetry> telemetryHistory = new List<Telemetry>();
        public CarID id;

        public Player(CarID id)
        {
            this.id = id;
        }

        public void Update(Telemetry telemetry)
        {
            telemetryHistory.Add(telemetry);
        }

        public Telemetry CurrentState
        {
            get { return telemetryHistory.Count > 0 ? telemetryHistory.Last() : null; }
        }

        public Telemetry PreviousState
        {
            get { return telemetryHistory.Count > 1 ? telemetryHistory[telemetryHistory.Count - 2] : null;  }   
        }
    }

    public class GameState
    {
        private const double MAX_ACCELERATION = 1.0; //Not really a known max, but just a limit to discard measured incorrect values which are above it.

        public Game Game;
        public DateTime CreationTime;

        // Defines the baseAcceleration of track, calculated with first ticks of game using max throttle
        private double baseAcceleration = double.MinValue;
        private bool baseAccelerationMeasured = false;
        private double baseAccelerationHighestMeasured = 0.0;
        private double baseAccelerationLowestMeasured = 0.0;

        public double GetBaseAcceleration()
        {
            if (baseAccelerationMeasured)
                return baseAcceleration;
            else
                return 0.0;
        }

        public List<TurboAvailable> turbosAvailable = new List<TurboAvailable>();
        public int nTurbosUsed;

        private bool raceStarted;

        public List<Player> Opponents { get; private set; }
        public Player Me { get; private set; }

        private Player FindPlayer(string name)
        {
            return Me.id.name.Equals(name) ? Me : Opponents.Find(o => o.id.name.Equals(name));
        }

        public bool RaceInProgress()
        {
            return raceStarted && Me.CurrentState != null;
        }

        public static int GetLaneDistanceFromCenter(CarPosition position, Track track)
        {
            //TODO: assuming same start and end lane index
            var laneData = position.piecePosition.lane;
            //if (laneData.endLaneIndex == laneData.startLaneIndex)
            //{
                return track.lanes.Find(lane => lane.index == laneData.startLaneIndex).distanceFromCenter;
            //}
        }

        private static double GetDistanceBetweenPositions(CarPosition curPos, CarPosition lastPos, Track track)
        {
            if (lastPos == null)
                return curPos.piecePosition.inPieceDistance;

            if (curPos.piecePosition.pieceIndex == lastPos.piecePosition.pieceIndex)
                return curPos.piecePosition.inPieceDistance - lastPos.piecePosition.inPieceDistance;

            return GetDistanceToPiece(track.pieces[curPos.piecePosition.pieceIndex], lastPos, track) + curPos.piecePosition.inPieceDistance;
        }

        // nTrackSwitches = how many times did/do we switch lanes in between.
        public static double GetDistanceToPiece(Piece toPiece, CarPosition fromPos, Track track)//, int nTrackSwitches)
        {
            //var curPiece = Game.race.track.pieces[fromPos.piecePosition.pieceIndex];
            Piece fromPiece = track.pieces[fromPos.piecePosition.pieceIndex];
            Piece curPiece = fromPiece;

            double distance = -fromPos.piecePosition.inPieceDistance; 
            while (curPiece.pieceId != toPiece.pieceId)
            {
                distance += curPiece.GetTrackLength(0);
                curPiece = track.NextPiece(curPiece);
            }
            /*
            double distance = 0;
            for (int i = fromPos.piecePosition.pieceIndex; i <= toPiece.pieceId; i++)
            {
                if (i == fromPos.piecePosition.pieceIndex)
                {
                    var trackLength = fromPiece.GetTrackLength(GetLaneDistanceFromCenter(fromPos, track));
                    var inPieceDistance = fromPos.piecePosition.inPieceDistance;

                    // Temporary hack for trackLength < inPieceDistance problem.
                    // Looks like this does not occure anymore, yay! But keeping the code here just to be safe..
                    distance += trackLength < inPieceDistance ? 0 : trackLength - inPieceDistance;
                }
                else if (i == toPiece.pieceId)
                {
                    ;
                }
                else
                {
                    distance += track.pieces[i].GetTrackLength(0); // TODO: don't know which lane we drove on here..
                }
            }
            */
            return distance;
        }

        internal void UpdateCarPosition(List<CarPosition> carPositions, int currentTick)
        {
            carPositions.ForEach(pos =>
            {
                var telemetry = new Telemetry(pos.id)
                {
                    angle = pos.angle,
                    piecePosition = pos.piecePosition,
                    tick = currentTick,
                    wallTime = DateTime.Now
                };

                if (pos.id != null)
                {
                    var player = FindPlayer(pos.id.name);
                    if (player != null)
                    {
                        var previous = player.CurrentState;
                        if (previous != null)
                        {
                            var tickDelta = telemetry.tick - previous.tick;
                            if (tickDelta != 0)
                            {
                                telemetry.speed = GetDistanceBetweenPositions(telemetry.CarPosition, previous.CarPosition, this.Game.race.track) / tickDelta;
                                telemetry.acceleration = (telemetry.speed - previous.speed) / tickDelta;

                                if (!baseAccelerationMeasured && telemetry.acceleration > 0.0)
                                {
                                    baseAccelerationMeasured = true;
                                    baseAcceleration = telemetry.acceleration;
                                }
                            }
                            
                            //TODO: hack to smoothen out incorrect speeds caused by incorrect distance measurements..
                            if (Math.Abs(telemetry.acceleration) > MAX_ACCELERATION)
                            {
                                telemetry.speed = previous.speed + (telemetry.acceleration > 0 ? MAX_ACCELERATION : -MAX_ACCELERATION);
                                telemetry.acceleration = (telemetry.speed - previous.speed) / tickDelta;
                            }
                        }

                        player.Update(telemetry);
                    }
                }
            });
        }

        internal void UpdateThrottleInTelemetry(double throttle)
        {
            Me.CurrentState.throttle = throttle;
        }

        public void NewTurboAvailable(TurboAvailable turboAvailable)
        {
            turbosAvailable.Add(turboAvailable);
        }

        public void StartRace()
        {
            this.raceStarted = true;
        }

        public void AssignMyCar(CarID myCar)
        {
            Me = new Player(myCar);
        }

        public void Initialize(Game game)
        {
            game.AssignTrackPieceIds();
            this.Game = game;
            this.CreationTime = DateTime.Now;

            CalculateBestTurboSpot(game);

            //Assign opponent cars
            Opponents = new List<Player>();
            Game.race.cars.ForEach(c =>
            {
                if (c.id.name != Me.id.name)
                    Opponents.Add(new Player(c.id));
            });

        }

        private void CalculateBestTurboSpot(Game game)
        {
            List<KeyValuePair<int, double>> Straights = new List<KeyValuePair<int, double>>();
            for (int ii = 0; ii < game.race.track.pieces.Count; ii++ )
            {
                Piece curPiece = game.race.track.pieces[ii];
                if (!curPiece.IsCurve())
                {
                    int nextPieceIndex = 0;
                    double straightLength = 0;
                    if (GetStraightLength(curPiece, game.race.track, out nextPieceIndex, out straightLength))
                    {
                        Straights.Add(new KeyValuePair<int,double>(curPiece.pieceId, straightLength));
                        if (nextPieceIndex < ii)
                            break;
                        ii = nextPieceIndex;
                    }
                }
            }

            KeyValuePair<int, double> bestStraight = new KeyValuePair<int,double>();
            foreach (KeyValuePair<int, double> curKVP in Straights)
            {
                if (curKVP.Value > bestStraight.Value)
                    bestStraight = curKVP;
            }

            if(bestStraight.Key >= 0)
            {
                game.race.track.pieces[bestStraight.Key].turboPiece = true;
            }
        }

        private bool GetStraightLength(Piece curPiece, Track track, out int nextPieceIndex, out double straightLength)
        {
            nextPieceIndex = -1;
            straightLength = 0.0;
            Piece nextPiece = curPiece;
            while (!nextPiece.IsCurve() || nextPiece.GetArcCurvature(0) < 0.2)
            {
                straightLength += curPiece.GetTrackLength(0);
                nextPiece = track.NextPiece(nextPiece);
            }
            nextPieceIndex = nextPiece.pieceId;
            return true;
        }

        public void Crashed(CarID crashedCar, int gameTick)
        {
            var player = FindPlayer(crashedCar.name);
            var t = player.CurrentState;
            player.Update(new Telemetry(crashedCar)
            {
                crashed = true,
                piecePosition = t.piecePosition,
                tick = gameTick
            });
        }

        public void Spawn(CarID car, int gameTick)
        {
            var player = FindPlayer(car.name);
            var t = player.CurrentState;
            player.Update(new Telemetry(car)
            {
                spawned = true,
                piecePosition = t.piecePosition,
                tick = gameTick
            });
        }

        public void TurboStart(CarID car, int gameTick)
        {
            var player = FindPlayer(car.name);
            var t = player.CurrentState;
            var newTele = new Telemetry(t)
            {
                turboActive = true,
                piecePosition = t.piecePosition,
                tick = gameTick
            };
            player.Update(newTele);
        }

        public void TurboEnd(CarID car, int gameTick)
        {
            var player = FindPlayer(car.name);
            var t = player.CurrentState;
            var newTele = new Telemetry(t)
            {
                turboActive = false,
                piecePosition = t.piecePosition,
                tick = gameTick
            };
            player.Update(newTele);
        }
    }
}
