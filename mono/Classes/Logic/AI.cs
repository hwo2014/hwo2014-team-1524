﻿
using System;
using System.Linq;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Messaging;
using TheAITeam.Classes.Tools;

namespace TheAITeam.Classes.Logic
{
    public abstract class AI
    {
        //protected const double maxAcceleration = 0.3; // seems to be this on default track;
        protected const double FRICTION = 0.1; // TODO: just a guess, figure out the real value for this
        //protected double enginePower = 1.0; // ??

        public GameState gameState = null;
        public abstract Messages.SendMsg CalculateReply();

        public void SetGameState(GameState gameState)
        {
            this.gameState = gameState;
        }

        protected double CalculateUpcomingStraightLength(Piece piece, CarPosition position)
        {
            double length = 0;
            Piece nextPiece = gameState.Game.race.track.NextPiece(piece);
            while (!nextPiece.IsCurve())
            {
                length += nextPiece.length;
                nextPiece = gameState.Game.race.track.NextPiece(nextPiece);
            }

            return length;
        }

        // Returns true if we need to break NOW in order to survive the next turn.
        protected bool BreakForCurve(CarPosition position, Piece piece, double friction)
        {
            var nextPiece = gameState.Game.race.track.NextPiece(piece);
            if (!nextPiece.IsCurve())
                return false;

            var speedDelta = gameState.Me.CurrentState.speed - CalculateTargetSpeed(nextPiece);
            var distanceToCurve = GetDistanceToNextCurve(position, piece);

            bool doBreak = distanceToCurve * friction <= speedDelta;

            return doBreak;
        }

        protected bool TurboAvailable()
        {
            return gameState.turbosAvailable.Count > gameState.nTurbosUsed;
        }

        protected bool ShouldUseTurbo(Piece curPiece, CarPosition position)
        {
            bool Result = false;
            if (TurboAvailable())
            {
                if (curPiece.turboPiece)
                {
                    Result = true;
                }
                else
                {
                    Piece nextPiece = gameState.Game.race.track.NextPiece(curPiece);
                    if (nextPiece.turboPiece)
                    {
                        Result = curPiece.GetTrackLengthForCurrentLane(position, gameState.Game.race.track) - position.piecePosition.inPieceDistance < 10.0;
                    }
                }
            }
            return Result;
        }
        /* Old turbo
        protected bool ShouldUseTurbo(Piece curPiece, CarPosition position)
        {
            bool Result = TurboAvailable();
            Result = Result & (!curPiece.IsCurve() || curPiece.IsCurve() && curPiece.GetTrackLengthForCurrentLane(position, gameState.Game.race.track) - position.piecePosition.inPieceDistance < 10.0);
            double carSpeed = gameState.Me.CurrentState.speed;
            Result = Result & CalculateUpcomingStraightLength(curPiece, position) >= carSpeed * 75;
            return Result;
        }*/

        protected Messages.SendMsg UseTurbo()
        {
            Logger.Write(Logger.Type.AIMessage, "Using turbo!");
            gameState.nTurbosUsed++;
            return new Messages.Turbo("Boost!!");
        }

        // TODO: take lane into account, SUPPORT FOR OLDER BOTS
        protected double CalculateTargetSpeed(Piece piece)
        {
            Lane curLane = null;
            return CalculateTargetSpeed(piece, curLane);
        }

        // Returns the optimal speed for driving through the given piece.
        protected double CalculateTargetSpeed(Piece piece, Lane curLane)
        {
            if (!piece.IsCurve())
                return Double.MaxValue;

            double laneDistanceFromCenter = curLane != null ? curLane.distanceFromCenter : 0.0;
            var curvature = Math.Abs(Curve.ArcCurvature(piece.radius + laneDistanceFromCenter, piece.angle)); // 45 degrees & 100 rad has curvature of 0.57..

            if (curvature < 0.1)
                return Double.MaxValue;
            else if (curvature < 0.2)
                return 10;
            else if (curvature < 0.3)
                return 8.5;
            else if (curvature < 0.4)
                return 7.5;
            else if (curvature < 0.5)
                return 7;
            else if (curvature < 0.6)
                return 6.25;
            else if (curvature < 0.7)
                return 6;
            else if (curvature < 0.8)
                return 5.5;
            else if (curvature < 0.9)
                return 5;
            else if (curvature < 1)
                return 4.5;
            else if (curvature < 1.1)
                return 4;
            else if (curvature < 1.2)
                return 3.5;
            else if (curvature < 1.3)
                return 3;
            else if (curvature < 1.4)
                return 2.5;
            else if (curvature < 1.5)
                return 2;
            else 
                return 1;
        }

        protected double GetDistanceToNextCurve(CarPosition position, Piece piece)
        {
            var nextCurve = gameState.Game.race.track.FindNextPiece(piece, p => p.IsCurve());
            if (nextCurve == null)
                return double.MaxValue;

            return GameState.GetDistanceToPiece(nextCurve, position, gameState.Game.race.track);
        }

        public virtual void Crashed()
        {
        }

        // Can mean the end of qualifying or game end
        public virtual void GameFinished()
        {
        }

        private double CalculateCurveLengthRight(Piece nextCurvePiece)
        {
            double CurveLength = 0.0;
            Piece nextPiece = nextCurvePiece;
            bool firstCurvePieceIsASwitch = nextPiece.isSwitch;
            while (!nextPiece.isSwitch || firstCurvePieceIsASwitch)
            {
                firstCurvePieceIsASwitch = false;// Only check this for the first piece
                if (nextPiece.angle > 0)
                {
                    CurveLength += nextPiece.GetTrackLength(0);
                }
                nextPiece = gameState.Game.race.track.NextPiece(nextPiece);
            }
            return CurveLength;
        }

        private double CalculateCurveLengthLeft(Piece nextCurvePiece)
        {
            double CurveLength = 0.0;
            Piece nextPiece = nextCurvePiece;
            bool firstCurvePieceIsASwitch = nextPiece.isSwitch;
            while (!nextPiece.isSwitch || firstCurvePieceIsASwitch)
            {
                firstCurvePieceIsASwitch = false;// Only check this for the first piece
                if (nextPiece.angle < 0)
                {
                    CurveLength += nextPiece.GetTrackLength(0);
                }
                nextPiece = gameState.Game.race.track.NextPiece(nextPiece);
            }
            return CurveLength;
        }

        private int FindInnerMostLaneForNextCurves(Piece curPiece)
        {
            var nextCurvePiece = gameState.Game.race.track.GetNextCurveTrackPiece(curPiece.pieceId);
            double CurveLengthLeft = CalculateCurveLengthLeft(nextCurvePiece);
            double CurveLengthRight = CalculateCurveLengthRight(nextCurvePiece);

            return CurveLengthRight > CurveLengthLeft ? gameState.Game.race.track.lanes[gameState.Game.race.track.lanes.Count - 1].index : gameState.Game.race.track.lanes[0].index;
        }

        private Piece lastSwitchPiece = null;
        protected bool SwitchLanes(Piece curPiece, CarPosition position, out string Direction)
        {
            Direction = "";
            // Check that we aren't already switching lanes
            if (position.piecePosition.lane.startLaneIndex == position.piecePosition.lane.endLaneIndex && lastSwitchPiece != curPiece)
            {
                int currentLane = position.piecePosition.lane.startLaneIndex;
                int bestLane = FindInnerMostLaneForNextCurves(curPiece);
                bool slowPokeInFront = CheckOvertake(curPiece, position, bestLane);
                if(slowPokeInFront && bestLane == currentLane)
                {
                    int overtakeLane = bestLane == 0 ? 1 : bestLane - 1;
                    lastSwitchPiece = curPiece;

                    if(overtakeLane == currentLane)
                        return false;

                    Direction = overtakeLane > currentLane ? "Right" : "Left";
                    return true;
                }
                else if (bestLane != currentLane)
                {
                    lastSwitchPiece = curPiece;

                    Direction = bestLane > currentLane ? "Right" : "Left";
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool CheckOvertake(Piece curPiece, CarPosition position, int bestLane)
        {
            bool ShouldOvertake = false;
            foreach(Player curPlayer in gameState.Opponents)
            {
                bool inSamePiece = curPlayer.CurrentState.piecePosition.pieceIndex == position.piecePosition.pieceIndex;
                if(inSamePiece)
                {
                    bool isAheadOfMe = curPlayer.CurrentState.piecePosition.inPieceDistance > position.piecePosition.inPieceDistance;
                    if(isAheadOfMe)
                    {
                        if(curPlayer.CurrentState.speed < gameState.Me.CurrentState.speed) // Slowbie ahead of us, prepare to overtake (maybe take into consideration how far ahead they are in piece at some point
                        {
                            ShouldOvertake = true;
                            break;
                        }
                    }
                }
            }
            return ShouldOvertake;
        }
        /* Use the other algorithm for now..
         * 
        // Returns true if we are now on a switch, and if by switching we would get a shorter track on the next turn.
        protected bool SwitchLanes(Piece piece, CarPosition position, out string direction)
        {
            if (piece.isSwitch)
            {
                var curve = gameState.Game.race.track.GetNextCurveTrackPiece(piece.pieceId);
                if (curve != null)
                {
                    var isLeftCurve =  curve.angle < 0;
                
                    var lanes = gameState.Game.race.track.lanes;//.OrderBy(l => l.distanceFromCenter).ToList();
                    var currentLane = position.piecePosition.lane;
                    var currentLaneOffset = lanes.Find(l => l.index == currentLane.startLaneIndex).distanceFromCenter;
                    var possibleLaneOffset = lanes.Find(l => l.index == currentLane.endLaneIndex).distanceFromCenter;

                    if (isLeftCurve && (possibleLaneOffset < currentLaneOffset))
                    {
                        direction = "Left";
                        return true;
                    }
                    else if(!isLeftCurve && (possibleLaneOffset > currentLaneOffset))
                    {
                        direction = "Right";
                        return true;
                    }
                }
            }
            direction = "";
            return false;
        }*/
    }
}
