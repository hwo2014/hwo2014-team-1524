﻿using System;
using System.Collections.Generic;
using System.Text;
using TheAITeam.Classes.Messaging;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Tools;

namespace TheAITeam.Classes.Logic
{
    public class AccelerationDatas
    {
        public List<AccelerationData> AccelerationDataList = new List<AccelerationData>();

        internal bool ShouldBreak(double curCarSpeed, double targetCarSpeed, double distanceToCurve, bool turboIsActive, out bool ShouldBreak)
        {
            ShouldBreak = false;
            // We assume throttle would be at 1.0 and we would turn it to 0.0
            double bestCarSpeedDelta = 0.0;
            AccelerationData ClosestMatchingAccelerationData = FindClosestMatchingAccelerationData(curCarSpeed, turboIsActive, out bestCarSpeedDelta);
            /*if (ClosestMatchingAccelerationData == null)
                return false;*/

            double newCarSpeed = ClosestMatchingAccelerationData == null ? curCarSpeed - 0.1 : ClosestMatchingAccelerationData.NewCarSpeed;
            RecursiveSpeedDistanceCheck(ClosestMatchingAccelerationData, curCarSpeed, targetCarSpeed, newCarSpeed, distanceToCurve, turboIsActive, out ShouldBreak);
            return true;
        }

        private void RecursiveSpeedDistanceCheck(AccelerationData ClosestMatchingAccelerationData, double curCarSpeed, double targetCarSpeed, double distanceTravelled, double distanceToCurve, bool turboIsActive, out bool ShouldBreak)
        {
            ShouldBreak = false;
            double bestCarSpeedDelta = 0.0;
            double acceleration = ClosestMatchingAccelerationData != null ? Math.Abs(ClosestMatchingAccelerationData.Acceleration) : 0.1;
            curCarSpeed -= acceleration;
            distanceTravelled += curCarSpeed;

            double safety = 1.0 + Math.Abs(bestCarSpeedDelta);
            if (distanceTravelled + safety >= distanceToCurve)
            {
                if (curCarSpeed >= targetCarSpeed)
                {
                    ShouldBreak = true;
                }
                else
                {
                    ShouldBreak = false;
                }
                return;
            }
            else if (distanceTravelled + safety < distanceToCurve)
            {
                if (curCarSpeed <= targetCarSpeed)
                {
                    ShouldBreak = false;
                    return;
                }
            }
            AccelerationData NewClosestMatchingAccelerationData = FindClosestMatchingAccelerationData(curCarSpeed, turboIsActive, out bestCarSpeedDelta);
            RecursiveSpeedDistanceCheck(NewClosestMatchingAccelerationData, curCarSpeed, targetCarSpeed, distanceTravelled, distanceToCurve, turboIsActive, out ShouldBreak);
        }

        private AccelerationData FindClosestMatchingAccelerationData(double curCarSpeed, bool turboIsActive, out double bestCarSpeedDelta)
        {
            AccelerationData ClosestMatchingAccelerationData = null;
            double carSpeedDelta = 0.0;
            bestCarSpeedDelta = double.MaxValue;
            foreach (AccelerationData curData in AccelerationDataList)
            {
                if (curData.LastThrottle == 0.0 && curData.Acceleration < 0.0 && curData.IsTurboActive == turboIsActive)
                {
                    carSpeedDelta = curData.LastCarSpeed - curCarSpeed;
                    if (carSpeedDelta < bestCarSpeedDelta)
                    {
                        bestCarSpeedDelta = carSpeedDelta;
                        ClosestMatchingAccelerationData = curData;
                    }
                }
            }

            return ClosestMatchingAccelerationData;
        }
    }

    public class AccelerationData
    {
        public double LastThrottle = 0.0;
        public double LastCarSpeed = 0.0;
        public double NewCarSpeed = 0.0;
        public bool IsTurboActive = false;

        public double Acceleration = 0.0;

        public AccelerationData(double lastThrottle, double lastCarSpeed, double curCarSpeed, bool turboActive)
        {
            this.LastThrottle = lastThrottle;
            this.LastCarSpeed = lastCarSpeed;
            this.NewCarSpeed = curCarSpeed;
            this.IsTurboActive = turboActive;

            this.Acceleration = NewCarSpeed - LastCarSpeed;
        }

        public override string ToString()
        {
            return string.Format("Throttle: {0}, {3}Acceleration: {1}, NewCarSpeed: {2}", LastThrottle, Acceleration, NewCarSpeed, IsTurboActive ? "TURBO, " : "");
        }
    }

    public class Speedy : AI
    {
        /// <summary>
        /// Maintains a list of target speeds for each track piece, this list will be optimized as we go along from the defaults, 
        /// while still maintaining slip within the limits of crashing.
        /// </summary>
        /// 
        public class LaneSpeedDetail
        {
            public List<double> SpeedList = null;
            public List<KeyValuePair<int, CurveSlipAngle>> CurveSlipAngles = new List<KeyValuePair<int, CurveSlipAngle>>();

            public LaneSpeedDetail(List<double> SpeedList)
            {
                this.SpeedList = SpeedList;
            }
        }

        public class CurveSlipAngle
        {
            public double Speed = 0.0;
            public double SlipAngle = 0.0;
            public double CurveAngle = 0.0;
            public double ArcCurvature = 0.0;
            public CurveSlipAngle(double Speed, double SlipAngle, double CurveAngle, double ArcCurvature)
            {
                this.Speed = Speed;
                this.SlipAngle = SlipAngle;
                this.CurveAngle = CurveAngle;
                this.ArcCurvature = ArcCurvature;
            }
        }

        public class LaneSpeedDetails
        {
            public List<KeyValuePair<int, LaneSpeedDetail>> ListOfLaneSpeedDetails = null;

            public List<KeyValuePair<int, CurveSlipAngle>> GetLaneCurveSlipAngles(int laneIndex)
            {
                return ListOfLaneSpeedDetails[laneIndex].Value.CurveSlipAngles;
            }

            public List<double> GetLaneSpeedList(int laneIndex)
            {
                return ListOfLaneSpeedDetails[laneIndex].Value.SpeedList;
            }
        }

        public LaneSpeedDetails laneSpeedDetails = null;
        private int lastUpdatedPieceSpeed = -1;
        private double lastThrottle = 0.0;
        private double lastCarSpeed = 0.0;
        private AccelerationDatas accelerationDatas = new AccelerationDatas();

        public override Messages.SendMsg CalculateReply()
        {
            if (laneSpeedDetails == null)
                SetupInitialSpeeds();

            var position = gameState.Me.CurrentState.CarPosition;
            Piece curPiece = gameState.Game.race.track.GetTrackPiece(position.piecePosition.pieceIndex);
            Piece nextPiece = gameState.Game.race.track.NextPiece(curPiece);

            UpdateCurrentCurveSlipAngle(gameState.Me.CurrentState.speed, position, curPiece);

            UpdateSpeedList(position, curPiece);

            double curCarSpeed = gameState.Me.CurrentState.speed;
            bool turboActive = gameState.Me.CurrentState.turboActive;
            if(curCarSpeed > 0.0) // Don't add crashes to list
                accelerationDatas.AccelerationDataList.Add(new AccelerationData(lastThrottle, lastCarSpeed, curCarSpeed, turboActive));
            lastCarSpeed = curCarSpeed;

            string Direction;
            Messages.SendMsg result = null;
            if (ShouldUseTurbo(curPiece, position))
            {
                result = UseTurbo();
            }
            else if (nextPiece.isSwitch && SwitchLanes(curPiece, position, out Direction))
            {
                if(!string.IsNullOrEmpty(Direction))
                    result = new Messages.SwitchLanes(Direction);
            }
            else
            {
                double newThrottle = CalculateThrottleBasedOnDistanceAndSpeedDifferences(curCarSpeed, curPiece, position);
                lastThrottle = newThrottle;
                result = new Messages.Throttle(newThrottle);
            }
            return result;
        }

        private void UpdateCurrentCurveSlipAngle(double carSpeed, CarPosition position, Piece curPiece)
        {
            if (curPiece.IsCurve())
            {
                List<KeyValuePair<int, CurveSlipAngle>> CurveSlipAngles = laneSpeedDetails.GetLaneCurveSlipAngles(position.piecePosition.lane.startLaneIndex);
                int IndexInList = CurveSlipAngles.FindIndex(KVP => KVP.Key == curPiece.pieceId);
                if (IndexInList >= 0)
                {
                    if (Math.Abs(position.angle) > CurveSlipAngles[IndexInList].Value.SlipAngle)
                    {
                        double maxSlipAngle = Math.Abs(position.angle);
                        CurveSlipAngles[IndexInList] = new KeyValuePair<int, CurveSlipAngle>(curPiece.pieceId, new CurveSlipAngle(carSpeed, maxSlipAngle, curPiece.angle, Math.Abs(Curve.ArcCurvature(curPiece.radius, curPiece.angle))));
                    }
                }
                else
                {
                    CurveSlipAngles.Add(new KeyValuePair<int, CurveSlipAngle>(curPiece.pieceId, new CurveSlipAngle(carSpeed, Math.Abs(position.angle), curPiece.angle, Math.Abs(Curve.ArcCurvature(curPiece.radius, curPiece.angle)))));
                }
            }
        }

        private void UpdateSpeedList(CarPosition position, Piece curPiece)
        {
            Piece previousPiece = gameState.Game.race.track.PreviousPiece(curPiece);
            if (previousPiece.IsCurve() && lastUpdatedPieceSpeed != previousPiece.pieceId)
            {
                // If previous pass through curve was too easy, lets speed it up some
                List<KeyValuePair<int, CurveSlipAngle>> CurveSlipAngles = laneSpeedDetails.GetLaneCurveSlipAngles(position.piecePosition.lane.endLaneIndex);
                int IndexInList = CurveSlipAngles.FindIndex(KVP => KVP.Key == previousPiece.pieceId);
                if (IndexInList >= 0)
                {
                    List<double> SpeedList = laneSpeedDetails.GetLaneSpeedList(position.piecePosition.lane.startLaneIndex);

                    double CurveSlipAngle = CurveSlipAngles[IndexInList].Value.SlipAngle;
                    if (CurveSlipAngle < 45)
                    {
                        SpeedList[previousPiece.pieceId] = Math.Max(SpeedList[previousPiece.pieceId], CurveSlipAngles[IndexInList].Value.Speed);
                    }

                    if(CurveSlipAngle < 15)
                        SpeedList[previousPiece.pieceId] *= 1.15; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 25)
                        SpeedList[previousPiece.pieceId] *= 1.05; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 30)
                        SpeedList[previousPiece.pieceId] *= 1.025; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 35)
                        SpeedList[previousPiece.pieceId] *= 1.008; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 40)
                        SpeedList[previousPiece.pieceId] *= 1.001; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 45)
                        SpeedList[previousPiece.pieceId] *= 1.0001; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle < 48)
                        SpeedList[previousPiece.pieceId] *= 1.00005; //TODO: calculate based on actual slipangle diff
                    else if(CurveSlipAngle > 50)
                        SpeedList[previousPiece.pieceId] *= 0.995; //TODO: calculate based on actual slipangle diff

                    if(CurveSlipAngle > 52)
                        SpeedList[previousPiece.pieceId] *= 0.99; //TODO: calculate based on actual slipangle diff

                    lastUpdatedPieceSpeed = previousPiece.pieceId;
                }
            }
        }

        private bool ShouldBreakForCurve(double curCarSpeed, CarPosition position, Piece piece)
        {
            Piece curvePiece = gameState.Game.race.track.GetNextCurveTrackPiece(piece.pieceId);

            List<double> SpeedList = laneSpeedDetails.GetLaneSpeedList(position.piecePosition.lane.endLaneIndex);
            double speedDelta = curCarSpeed - SpeedList[curvePiece.pieceId];
            double distanceToCurve = GetDistanceToNextCurve(position, piece);

            bool ShouldBreak = false;
            if (accelerationDatas.ShouldBreak(curCarSpeed + gameState.GetBaseAcceleration(), SpeedList[curvePiece.pieceId], distanceToCurve, gameState.Me.CurrentState.turboActive, out ShouldBreak))
            {
                if (!ShouldBreak)
                {
                    Piece secondCurvePiece = gameState.Game.race.track.GetNextCurveTrackPiece(curvePiece.pieceId);
                    double distanceToSecondCurve = GameState.GetDistanceToPiece(secondCurvePiece, position, gameState.Game.race.track);
                    bool ShouldBreakForSecondCurve = false;
                    if (accelerationDatas.ShouldBreak(curCarSpeed + gameState.GetBaseAcceleration(), SpeedList[secondCurvePiece.pieceId], distanceToSecondCurve, gameState.Me.CurrentState.turboActive, out ShouldBreakForSecondCurve))
                    {
                        return ShouldBreakForSecondCurve;
                    }
                }
                return ShouldBreak;
            }
            else
            {
                // Fallback if we don't have any suitable accelerationdata yet
                bool doBreak = distanceToCurve * FRICTION * (curCarSpeed / 10) <= speedDelta * Math.Max(Math.Abs(speedDelta), 4);
                return doBreak;
            }
            return false;
        }

        private double CalculateThrottleBasedOnDistanceAndSpeedDifferences(double curCarSpeed, Piece curPiece, CarPosition position)
        {
            List<double> SpeedList = laneSpeedDetails.GetLaneSpeedList(position.piecePosition.lane.startLaneIndex);
            double targetPieceSpeed = SpeedList[position.piecePosition.pieceIndex];

            bool checkSpeedIncrease = false;
            double newThrottle = 0.3;
            if (Math.Abs(targetPieceSpeed - curCarSpeed) < gameState.GetBaseAcceleration()) // We are close to the target speed, lets increase throttle gently
            {
                double speedDifference = targetPieceSpeed - curCarSpeed;
                newThrottle = 0.3;
                if(speedDifference > 0.0) // We need to increase speed slightly
                    newThrottle *= 1 + (gameState.GetBaseAcceleration() - speedDifference) / gameState.GetBaseAcceleration();
                else // We need to decrease speed slightly
                    newThrottle *= (gameState.GetBaseAcceleration() - Math.Abs(speedDifference)) / gameState.GetBaseAcceleration();

                if (newThrottle > 1.0)
                    newThrottle = 1.0;
                else if (newThrottle < 0.0)
                    newThrottle = 0.0;

                if (ShouldBreakForCurve(curCarSpeed, position, curPiece))
                    newThrottle = 0.0;
                else
                    checkSpeedIncrease = true;
            }
            else if (curCarSpeed < targetPieceSpeed) // We not near max accel, lets floor it
            {
                newThrottle = 1.0; // ACCELERATE

                if (ShouldBreakForCurve(curCarSpeed, position, curPiece))
                {
                    newThrottle = 0.0;
                    checkSpeedIncrease = true;
                }
            }
            else if (curCarSpeed > targetPieceSpeed)
            {
                newThrottle = 0.0; // BREAK
                checkSpeedIncrease = true;
            }

            // Try to increase speed through curves if we are going way too slow
            Piece nextPiece = gameState.Game.race.track.NextPiece(curPiece);
            double nextPieceSpeed = SpeedList[nextPiece.pieceId];
            if (checkSpeedIncrease == true && curPiece.IsCurve() && curCarSpeed <= nextPieceSpeed)
            {
                // Check if we through apex
                if (position.piecePosition.inPieceDistance > curPiece.GetTrackLengthForCurrentLane(position, gameState.Game.race.track) * 0.5)
                {
                    Lane curLane = gameState.Game.race.track.lanes[position.piecePosition.lane.endLaneIndex];
                    double ArcCurvature = curPiece.GetArcCurvature(curLane.distanceFromCenter);

                    double maxSlip = 15;
                    if (ArcCurvature > 0.8)
                        maxSlip = 5;
                    else if (ArcCurvature > 1.0)
                        maxSlip = 1;

                    if (Math.Abs(position.angle) < maxSlip)
                        newThrottle = 1.0;
                }
            }

            return newThrottle;
        }

        public override void Crashed()
        {
            CarPosition position = gameState.Me.CurrentState.CarPosition;
            Piece curPiece = gameState.Game.race.track.GetTrackPiece(position.piecePosition.pieceIndex);
            double curCarSpeed = gameState.Me.CurrentState.speed;
            List<double> SpeedList = laneSpeedDetails.GetLaneSpeedList(position.piecePosition.lane.startLaneIndex);
            double lastTargetSpeed = SpeedList[position.piecePosition.pieceIndex];
            SpeedList[position.piecePosition.pieceIndex] *= 0.9;
            List<KeyValuePair<double, double>> AngleSpeeds = GetAngleSpeeds();
            return;
        }

        public override void GameFinished()
        {
            List<KeyValuePair<double, double>> AngleSpeeds = GetAngleSpeeds();
            // Lets check what actual speeds we could travel the curves with, these should be used as default values for the main race
            return;
        }

        private List<KeyValuePair<double, double>> GetAngleSpeeds()
        {
            List<KeyValuePair<double, double>> AngleSpeeds = new List<KeyValuePair<double, double>>();
            foreach (Piece curPiece in gameState.Game.race.track.pieces)
            {
                if (curPiece.IsCurve())
                {
                    for (int ii = 0; ii < gameState.Game.race.track.lanes.Count; ii++)
                    {
                        Lane curLane = gameState.Game.race.track.lanes[ii];
                        List<double> SpeedList = laneSpeedDetails.GetLaneSpeedList(ii);
                        var curvature = Math.Abs(Curve.ArcCurvature(curPiece.radius + curLane.distanceFromCenter, curPiece.angle));
                        AngleSpeeds.Add(new KeyValuePair<double, double>(curvature, SpeedList[curPiece.pieceId]));
                    }
                }
            }
            return AngleSpeeds;
        }

        private void SetupInitialSpeeds()
        {
            laneSpeedDetails = new LaneSpeedDetails();
            laneSpeedDetails.ListOfLaneSpeedDetails = new List<KeyValuePair<int, LaneSpeedDetail>>();
            for(int ii = 0; ii < gameState.Game.race.track.lanes.Count; ii++)
            {
                Lane curLane = gameState.Game.race.track.lanes[ii];
                List<double> SpeedList = new List<double>();
                foreach (Piece curPiece in gameState.Game.race.track.pieces)
                {
                    SpeedList.Add(CalculateTargetSpeed(curPiece, curLane));
                }
                laneSpeedDetails.ListOfLaneSpeedDetails.Add(new KeyValuePair<int, LaneSpeedDetail>(ii, new LaneSpeedDetail(SpeedList)));
            }
        }
    }
}
