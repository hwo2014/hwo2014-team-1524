﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheAITeam.Classes.Messaging;

namespace TheAITeam.Classes.Logic
{
    class Crashy : AI
    {
        public override Messages.SendMsg CalculateReply()
        {
            return new Messages.Throttle(0.7);
        }
    }
}
