﻿using System;
using System.Collections.Generic;

public class Position
{
    public double x;
    public double y;
}

public class StartingPoint
{
    public Position position;
    public double angle;
}

public class Track
{
    public string id;
    public string name;
    public List<Piece> pieces;
    public List<Lane> lanes;
    public StartingPoint startingPoint;

    public Piece GetTrackPiece(int trackPieceId)
    {
        return pieces.Find(p => p.pieceId == trackPieceId);
    }

    public Piece GetNextCurveTrackPiece(int trackPieceId)
    {
        return FindNextPiece(GetTrackPiece(trackPieceId), p => p.IsCurve());
    }

    public Piece NextPiece(Piece piece)
    {
        int lastPieceId = pieces.Count - 1;
        return piece.pieceId == lastPieceId ? pieces[0] : pieces[piece.pieceId + 1];
    }

    public Piece PreviousPiece(Piece piece)
    {
        int firstPieceId = 0;
        return piece.pieceId == firstPieceId ? pieces[pieces.Count-1] : pieces[piece.pieceId - 1];
    }

    public Piece FindNextPiece(Piece currentPiece, Func<Piece, bool> condition)
    {
        Piece nextPiece = NextPiece(currentPiece);
        while (!condition(nextPiece) && currentPiece.pieceId != nextPiece.pieceId)
            nextPiece = NextPiece(nextPiece);

        return nextPiece.pieceId == currentPiece.pieceId ? null : nextPiece;
    }
}

