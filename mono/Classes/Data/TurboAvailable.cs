﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAITeam.Classes.Data
{
    public class TurboAvailable
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }
}
