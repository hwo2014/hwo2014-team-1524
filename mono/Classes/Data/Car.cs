﻿
using System.Collections.Generic;

public class CarID
{
    public string name;
    public string color;
}

public class CarDimensions
{
    public double length;
    public double width;
    public double guideFlagPosition;
}

public class CarPosition
{
    public CarID id;
    public double angle = 0.0;
    public PiecePosition piecePosition;
}

/*
[
  {
    "id": {
      "name": "The AI Team",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  }
]
*/

public class Car
{
    public CarID id;
    public CarDimensions dimensions;
}
