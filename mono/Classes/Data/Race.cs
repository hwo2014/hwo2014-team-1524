﻿using System.Collections.Generic;

public class Game
{
    public Race race;

    public void AssignTrackPieceIds()
    {
        for (int ii = 0; ii < race.track.pieces.Count; ii++)
        {
            race.track.pieces[ii].pieceId = ii;
        }
    }
}

public class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;
}

public class Race
{
    public Track track;
    public List<Car> cars;
    public RaceSession raceSession;

    public Car GetCar(CarID car)
    {
        foreach(Car curCar in cars)
        {
            if(curCar.id == car)
                return curCar;
        }
        return null;
    }
}
