﻿
using TheAITeam.Classes.Tools;
using Newtonsoft.Json;
using TheAITeam.Classes.Data;
using System;

public class Piece
{
    public int pieceId = 0;
    public double length;

    [JsonProperty("switch")]
    public bool isSwitch;
    public double angle;
    public double radius;
    public bool turboPiece = false;

    internal bool IsCurve()
    {
        return radius > 0.0;
    }

    public double GetTrackLengthForCurrentLane(CarPosition position, Track track)
    {
        return GetTrackLength(GameState.GetLaneDistanceFromCenter(position, track));
    }

    public double GetTrackLength(int laneDistance)
    {
        if (IsCurve())
        {
            // In left turns (negative angle), lanes with negative offset are in the inner curve, and vice versa.
            return Curve.ArcLength(radius + (angle < 0 ? laneDistance : -laneDistance), angle);
        }
        if (isSwitch)
        {
            return length + 1.98;  // Apparently, switch pieces are longer even if you don't swithc lanes..!?!?
        }
        
        return length;
        
    }

    public override string ToString()
    {
        return string.Format("ID: {0} - Length: {1}, angle: {2}, Radius: {3} {4}", pieceId, length, angle, radius, isSwitch ? "<Switch>" : "");
    }

    internal double GetArcCurvature(double laneDistanceFromCenter)
    {
        var curvature = Math.Abs(Curve.ArcCurvature(radius + laneDistanceFromCenter, angle)); // 45 degrees & 100 rad has curvature of 0.57..
        return curvature;
    }
}

public class LanePosition
{
    public int startLaneIndex;
    public int endLaneIndex;
}

public class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public LanePosition lane;
    public int lap;
}