﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheAITeam.Classes.Data
{
    class Stats
    {
        public int lap;
        public int ticks;
        public int millis;
    }

    class Ranking
    {
        public int overall;
        public int fastestLap;
    }

    class LapFinished
    {
        public CarID car;
        public Stats lapTime;
        public Stats raceTime;
        public int overall;
        public int fastestLap;
    }
}
