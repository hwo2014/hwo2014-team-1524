using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

using TheAITeam.Classes.Data;
using TheAITeam.Classes.Logic;
using TheAITeam.Classes.Messaging;
using TheAITeam.Classes;

public class Bot
{
    public const string DEFAULT_HOST = "hakkinen.helloworldopen.com";
    public const int DEFAULT_PORT = 8091;
    public const string DEFAULT_BOTNAME = "The AI Team";
    public const string DEFAULT_BOTKEY = "HKzS4vn5dR1bLQ";

    public static void Start(bool replay, bool logTofile, string host, int port, AI ai, IVisualizer visualizer, Messages.SendMsg joinMsg)
    {
        using (TcpClient client = new TcpClient(host, port))
        {
            StreamReader reader;
            StreamWriter writer;

            if (replay)
            {
                string file = Directory.GetFiles("logs").OrderByDescending(f => File.GetLastWriteTime(f)).First();
                reader = new LogReader(new FileStream(file, FileMode.Open));
                writer = new DummyWriter(new MemoryStream());
                logTofile = false;
            }
            else
            {
                NetworkStream stream = client.GetStream();
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;
            }
            
            Logger.LogToFile(logTofile
                ? new List<Logger.Type> { Logger.Type.Receive, Logger.Type.Send }
                : new List<Logger.Type>());
            Logger.LogToScreen(new List<Logger.Type> { Logger.Type.AIMessage, Logger.Type.Info });

            new GameLoop(ai, reader, writer, visualizer).Start(joinMsg);
        }
    }

    public static void Start(AI ai, IVisualizer visualizer, Messages.SendMsg joinMsg, bool replayLog)
    {
        Start(replayLog, true, DEFAULT_HOST, DEFAULT_PORT, ai, visualizer, joinMsg);
    }
}