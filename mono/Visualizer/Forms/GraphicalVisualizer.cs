﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Media;
using TheAITeam.Classes.Data;
using TheAITeam.Classes.Logic;
using TheAITeam.Classes.Messaging;
using TheAITeam.Classes.Tools;
using Color = System.Drawing.Color;
using Matrix = System.Drawing.Drawing2D.Matrix;
using Pen = System.Drawing.Pen;
using Point = Visualizer.Forms.Point;

namespace TheAITeam.Forms
{
    using global::Visualizer.Forms;

    using TheAITeam.Classes;

    class GraphicalVisualizer : IVisualizer, Logger.IScreenWriter
    {
        private bool isAlive = true;
        public Forms.Visualizer formVisualizer = new Visualizer();

        private static void Start(Messages.SendMsg joinMsg, string AIName)
        {
            Start(joinMsg, AIName, false);
        }
        private static void Start(Messages.SendMsg joinMsg, string AIName, bool replay)
        {
            var type = Type.GetType(typeof(Slippy).AssemblyQualifiedName.Replace("Slippy", AIName));
            AI ai = (AI)Activator.CreateInstance(type);
            var visualizer = new GraphicalVisualizer();
            visualizer.Init();

            if(replay)
                Bot.Start(true, false, Bot.DEFAULT_HOST, Bot.DEFAULT_PORT, ai, visualizer, joinMsg);
            else
                Bot.Start(false, true, Bot.DEFAULT_HOST, Bot.DEFAULT_PORT, ai, visualizer, joinMsg);
            
        }

        [STAThread]
        static void Main(string[] args)
        {
            string startType = "join";
            string trackName = "usa";
            string playerCount = "1";
            string AIName = "Speedy";
            string password = "TheAITeamRacePassword1";
            
            if (args.Length == 1 && args[0].Equals("replay"))
            {
                Start(new Messages.Ping(), AIName, true);
            }
            else
            {
                if (args.Length > 0)
                {
                    startType = args[0];
                    trackName = args[1];
                    playerCount = args[2];
                    AIName = args[3];
                    password = args.Length > 3 ? args[4] : "";
                }

                if (password.Length == 0)
                {
                    var msg = new Messages.JoinRace()
                    {
                        botId = new Messages.JoinRace.BotID()
                        {
                            key = Bot.DEFAULT_BOTKEY,
                            name = AIName + (new Random()).Next(100)
                        },
                        carCount = int.Parse(playerCount),
                        trackName = trackName
                    };
                    Start(msg, AIName);
                }
                else
                {
                    var msg = new Messages.JoinRaceWithPassword();
                    msg.botId = new Messages.JoinRace.BotID()
                    {
                        key = Bot.DEFAULT_BOTKEY,
                        name = AIName + (new Random()).Next(100)
                    };
                    msg.password = password;
                    msg.carCount = int.Parse(playerCount);
                    msg.trackName = trackName;

                    Start(msg, AIName);
                }
            }
        }

        public void Init()
        {
            formVisualizer.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VisualizerFormUnload);   
            formVisualizer.Show();

            Logger.SetLogToScreenWriter(this);
        }

        public void InitGraphics(GameState state)
        {
            if (!isAlive)
                return;

            this.CalculateTrackPositions(state);
        }

        public bool IsAlive()
        {
            return isAlive;
        }

        public void DoEvents()
        {
            if (!isAlive)
                return;

            System.Windows.Forms.Application.DoEvents();
        }

        public void VisualizeRaceState(GameState state, Messages.SendMsg msg)
        {
            if (!isAlive)
                return;

            double throttle = msg as Messages.Throttle == null ? 0 : ((Messages.Throttle) msg).value;

            VisualizeRaceState(state);
            VisualizeLinearState(state, throttle);
            VisualizeTelemetry(state);
            UpdateStatusBox(state, throttle);
        }

        private void VisualizeTelemetry(GameState state)
        {
            var bmp = new Bitmap(formVisualizer.picTelemetry.Width, formVisualizer.picTelemetry.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.White);
                var tickInLap = -1;
                var tickWidth = 1;

                for (int i = 0; i < state.Me.telemetryHistory.Count; i++)
                {
                    tickInLap = (i == 0 || state.Me.telemetryHistory[i].piecePosition.lap == state.Me.telemetryHistory[i - 1].piecePosition.lap)
                        ? ++tickInLap
                        : 0;

                    var x1 = tickInLap*tickWidth;
                    var x2 = (tickInLap + 1)*tickWidth;

                    if(formVisualizer.chkSpeed.Checked)
                    {
                        var to = state.Me.telemetryHistory[i].speed;
                        var from = i == 0 ? 0 : state.Me.telemetryHistory[i - 1].speed;
                        var scale = bmp.Height/10.0; //maxspeed
                        g.DrawLine(Pens.Black, (int) x1, (int) (bmp.Height - from*scale), (int) x2,
                            (int) (bmp.Height - to*scale));

                        if(i == state.Me.telemetryHistory.Count - 1)
                            g.DrawString(String.Format("{0:0.0}", to), new Font("Arial", 8), new SolidBrush(Color.Black), (int) x2,
                                (int) (bmp.Height - to*scale));
                    }
                    if(formVisualizer.chkThrottle.Checked)
                    {
                        var to = state.Me.telemetryHistory[i].throttle;
                        var from = i == 0 ? 0 : state.Me.telemetryHistory[i - 1].throttle;
                        var scale = bmp.Height/1.0; //maxthrottle
                        g.DrawLine(Pens.Bisque, (int)x1, (int)(bmp.Height - from * scale), (int)x2,
                            (int)(bmp.Height - to * scale));

                        if (i == state.Me.telemetryHistory.Count - 1)
                            g.DrawString(String.Format("{0:0.0}", to), new Font("Arial", 8), new SolidBrush(Color.Bisque), (int)x2,
                                (int)(bmp.Height - to * scale));
                    }
                    if(formVisualizer.chkSlip.Checked)
                    {
                        var to = Math.Abs(state.Me.telemetryHistory[i].angle);
                        var from = Math.Abs(i == 0 ? 0 : state.Me.telemetryHistory[i - 1].angle);
                        var scale = bmp.Height / 60.0; //maxslip
                        g.DrawLine(Pens.Red, (int)x1, (int)(bmp.Height - from * scale), (int)x2,
                            (int)(bmp.Height - to * scale));

                        if (i == state.Me.telemetryHistory.Count - 1)
                            g.DrawString(String.Format("{0:0.0}", to), new Font("Arial", 8), new SolidBrush(Color.Red), (int)x2,
                                (int)(bmp.Height - to * scale));
                    }
                    if (formVisualizer.chkAcceleration.Checked)
                    {
                        var to = Math.Abs(state.Me.telemetryHistory[i].acceleration);
                        var from = Math.Abs(i == 0 ? 0 : state.Me.telemetryHistory[i - 1].acceleration);
                        var scale = bmp.Height / 0.1; //maxacc
                        g.DrawLine(Pens.Blue, (int)x1, (int)(bmp.Height - from * scale), (int)x2,
                            (int)(bmp.Height - to * scale));

                        if (i == state.Me.telemetryHistory.Count - 1)
                            g.DrawString(String.Format("{0:0.0}", to), new Font("Arial", 8), new SolidBrush(Color.Blue), (int)x2,
                                (int)(bmp.Height - to * scale));
                    }
                }
            }
            formVisualizer.picTelemetry.Image = bmp;
        }

        public void VisualizeLinearState(GameState state, double throttle)
        {
            var pieces = state.Game.race.track.pieces;

            var bmp = new Bitmap(pieces.Count, 10);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.White);

                var maxAngle = 180.0;
                pieces.ForEach(p =>
                {
                    var a = Math.Abs(p.angle) * (255.0 / maxAngle);
                    var color = Color.FromArgb(Math.Min((int)a, 255), 255, 0, 0);
                    var brush = new SolidBrush(color);
                    g.FillRectangle(brush, p.pieceId, 0, 1, 10);
                });
            }

            formVisualizer.picLinear.Image = bmp;
        }

        public void VisualizeRaceState(GameState gameState)
        {
            Bitmap bmp = new Bitmap(formVisualizer.pictureBox_Canvas.Width, formVisualizer.pictureBox_Canvas.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.Black);
                DrawTrack(g, gameState);
                DrawCars(g, gameState);
            }
            formVisualizer.pictureBox_Canvas.Image = bmp;
        }

        private double lastSpeedBeforeCrash = -1;
        private void UpdateStatusBox(GameState state, double throttle)
        {
            var s = formVisualizer.txtStatus;
            s.Clear();

            var myCar = state.Me.id;
            var piece = state.Game.race.track.pieces[state.Me.CurrentState.CarPosition.piecePosition.pieceIndex];

            s.AppendText(myCar.name + "\n");
            s.AppendText("Car slip: " + String.Format("{0:0.00}", state.Me.CurrentState.CarPosition.angle) + "\n");

            var lastCrash = state.Me.telemetryHistory.FindLast(t => t.crashed);
            if (lastCrash != null && state.Me.CurrentState.tick - lastCrash.tick < 60 * 5)
            {
                if(lastSpeedBeforeCrash < 0)
                    lastSpeedBeforeCrash = state.Me.PreviousState.speed;
                s.AppendText("Crashed: " + String.Format("{0:0.00} lastSpeed: {1}", state.Me.CurrentState.angle, lastSpeedBeforeCrash) + "\n");
            }
            else
            {
                lastSpeedBeforeCrash = -1;
            }

            s.AppendText("Throttle: " + String.Format("{0:0.00}", throttle) + "\n");
            s.AppendText("Speed:" +  String.Format("{0:0.00}", state.Me.CurrentState.speed) + "\n");
            s.AppendText("Acceleration:" + String.Format("{0:0.00}", state.Me.CurrentState.acceleration) + "\n");
            var curvature = Math.Abs(Curve.ArcCurvature(piece.radius, piece.angle));
            s.AppendText("Piece length: " + String.Format("{0:0.00}, angle: {1} radius: {2} arcCurve: {3}", piece.GetTrackLength(0), piece.angle, piece.radius, curvature));
        }

        private void DrawCars(Graphics g, GameState gameState)
        {
            foreach(Car curCar in gameState.Game.race.cars)
            {
                var position = gameState.Me.CurrentState.CarPosition;

                var curTrackPiece = gameState.Game.race.track.GetTrackPiece(position.piecePosition.pieceIndex) as PieceWithPosition;
                Point trackPieceStartPoint = curTrackPiece.position.startPoint;
                Point trackPieceEndPoint = curTrackPiece.position.endPoint;
                Vector trackPieceMoveVector = new Vector(trackPieceStartPoint, trackPieceEndPoint);
                trackPieceMoveVector.Normalize(position.piecePosition.inPieceDistance);
                Point carPosition = new Point(trackPieceStartPoint.X + trackPieceMoveVector.X, trackPieceStartPoint.Y + trackPieceMoveVector.Y);
                g.DrawRectangle(GetCarPen(curCar), (float)carPosition.X, (float)carPosition.Y, 10, 10);
            }
        }

        private Pen GetCarPen(Car curCar)
        {
            switch(curCar.id.color)
            {
                case "red":
                    return Pens.Red;
            }
            return Pens.White;
        }

        private void DrawTrack(Graphics g, GameState gameState)
        {
            GraphicsPath path = new GraphicsPath();
            foreach (PieceWithPosition curPiece in gameState.Game.race.track.pieces)
            {
                Point startPoint = curPiece.position.startPoint;
                Point endPoint = curPiece.position.endPoint;
                if (curPiece.angle == 0)
                {
                    path.AddLine((int)startPoint.X, (int)startPoint.Y, (int)endPoint.X, (int)endPoint.Y);
                }
                else
                {
                    path.AddLine((int)startPoint.X, (int)startPoint.Y, (int)endPoint.X, (int)endPoint.Y);
                    // This is not working correctly, not even remotely, to be fixed!
                    //double startAngle = curPiece.position.moveVector.GetXYVectorAngle();
                    //path.AddArc((float)startPoint.X, (float)startPoint.Y, (float)Math.Abs(endPoint.X - startPoint.X), (float)Math.Abs(endPoint.Y - startPoint.Y), (float)startAngle, (float)curPiece.angle);
                }

                if (curPiece.isSwitch)
                {
                    path.AddRectangle(new Rectangle((int)startPoint.X, (int)startPoint.Y, (int)endPoint.X - (int)startPoint.X, (int)endPoint.Y - (int)startPoint.Y));
                }
            }
            RectangleF graphicsSize = path.GetBounds();
            Matrix transform = new Matrix();
            transform.Scale(g.VisibleClipBounds.Height / graphicsSize.Width, g.VisibleClipBounds.Height / graphicsSize.Height);
            transform.Translate(g.VisibleClipBounds.Left - graphicsSize.Left, g.VisibleClipBounds.Top - graphicsSize.Top);
            g.Transform = transform;
            g.DrawPath(Pens.Gray, path);
            g.DrawLine(Pens.Red, new PointF(1.0f, 1.0f), new PointF(100f, 100f));
            g.Flush();
        }

        public void VisualizerFormUnload(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            //System.Windows.Forms.Application.Exit();
            //Environment.Exit(0);
            isAlive = false;
        }

        internal void CalculateTrackPositions(GameState state)
        {
            var race = state.Game.race;
            Point startPoint = new Point(race.track.startingPoint.position.x, race.track.startingPoint.position.y);
            Point endPoint;
            Vector lastPieceMoveVector = CalculateStartVector(startPoint, race.track.pieces[0]);
            for (int ii = 0; ii < race.track.pieces.Count; ii++)
            {
                PieceWithPosition curPiece = new PieceWithPosition(race.track.pieces[ii]);
                curPiece.position = new PositionData();
                curPiece.position.startPoint = startPoint;
                if (Math.Abs(curPiece.radius) > 0.0)
                {
                    Vector orthoVector;
                    Point curveCenterPoint;
                    if (curPiece.angle < 0.0)
                    {
                        orthoVector = lastPieceMoveVector.PositiveOrtho();
                        orthoVector.Normalize(curPiece.radius);
                        curveCenterPoint = new Point(startPoint.X + orthoVector.X, startPoint.Y + orthoVector.Y);
                    }
                    else
                    {
                        orthoVector = lastPieceMoveVector.PositiveOrtho();
                        orthoVector.Normalize(curPiece.radius);
                        curveCenterPoint = new Point(startPoint.X - orthoVector.X, startPoint.Y - orthoVector.Y);
                    }

                    Vector newEndPointVector = Vector.RotateVector(orthoVector, curPiece.angle);
                    newEndPointVector.Normalize(curPiece.radius);

                    endPoint = new Point(curveCenterPoint.X + newEndPointVector.X, curveCenterPoint.Y + newEndPointVector.Y);

                    Vector newEndPointVectorForMoveVector = Vector.RotateVector(orthoVector, curPiece.angle + 1);
                    newEndPointVectorForMoveVector.Normalize(curPiece.radius);
                    Point newEndPointForMoveVector = new Point(curveCenterPoint.X + newEndPointVectorForMoveVector.X, curveCenterPoint.Y + newEndPointVectorForMoveVector.Y);

                    curPiece.position.moveVector = new Vector(endPoint, newEndPointForMoveVector);
                    lastPieceMoveVector = curPiece.position.moveVector;
                    lastPieceMoveVector.Normalize(1.0);
                }
                else
                {
                    Vector moveVector = Vector.RotateVector(lastPieceMoveVector, curPiece.angle);
                    moveVector.Normalize(curPiece.length);
                    endPoint = new Point(startPoint.X + moveVector.X, startPoint.Y + moveVector.Y);
                    curPiece.position.moveVector = new Vector(startPoint, endPoint);
                    lastPieceMoveVector = curPiece.position.moveVector;
                }
                curPiece.position.endPoint = endPoint;
                startPoint = endPoint;

                race.track.pieces[ii] = curPiece;
            }
        }

        private Vector CalculateStartVector(Point startPoint, Piece piece)
        {
            return new Vector(1, 0); // This should take into account possible different rotations and or corners as start piece
        }

        public void WriteLine(string s)
        {
            formVisualizer.txtLogs.AppendText(s + "\n");
        }
    }
}
