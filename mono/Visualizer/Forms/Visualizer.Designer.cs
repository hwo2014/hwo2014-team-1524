﻿namespace TheAITeam.Forms
{
    partial class Visualizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_Canvas = new System.Windows.Forms.PictureBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.picLinear = new System.Windows.Forms.PictureBox();
            this.picTelemetry = new System.Windows.Forms.PictureBox();
            this.chkThrottle = new System.Windows.Forms.CheckBox();
            this.chkSlip = new System.Windows.Forms.CheckBox();
            this.chkSpeed = new System.Windows.Forms.CheckBox();
            this.chkAcceleration = new System.Windows.Forms.CheckBox();
            this.txtLogs = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Canvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLinear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTelemetry)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_Canvas
            // 
            this.pictureBox_Canvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Canvas.Location = new System.Drawing.Point(12, 411);
            this.pictureBox_Canvas.Name = "pictureBox_Canvas";
            this.pictureBox_Canvas.Size = new System.Drawing.Size(1212, 339);
            this.pictureBox_Canvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_Canvas.TabIndex = 0;
            this.pictureBox_Canvas.TabStop = false;
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.Location = new System.Drawing.Point(12, 12);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(367, 93);
            this.txtStatus.TabIndex = 1;
            // 
            // picLinear
            // 
            this.picLinear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picLinear.Location = new System.Drawing.Point(13, 111);
            this.picLinear.Name = "picLinear";
            this.picLinear.Size = new System.Drawing.Size(1212, 11);
            this.picLinear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLinear.TabIndex = 2;
            this.picLinear.TabStop = false;
            // 
            // picTelemetry
            // 
            this.picTelemetry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picTelemetry.Location = new System.Drawing.Point(12, 151);
            this.picTelemetry.Name = "picTelemetry";
            this.picTelemetry.Size = new System.Drawing.Size(1212, 254);
            this.picTelemetry.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTelemetry.TabIndex = 4;
            this.picTelemetry.TabStop = false;
            // 
            // chkThrottle
            // 
            this.chkThrottle.AutoSize = true;
            this.chkThrottle.Location = new System.Drawing.Point(13, 128);
            this.chkThrottle.Name = "chkThrottle";
            this.chkThrottle.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkThrottle.Size = new System.Drawing.Size(58, 17);
            this.chkThrottle.TabIndex = 5;
            this.chkThrottle.Text = "throttle";
            this.chkThrottle.UseVisualStyleBackColor = true;
            // 
            // chkSlip
            // 
            this.chkSlip.AutoSize = true;
            this.chkSlip.Checked = true;
            this.chkSlip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSlip.Location = new System.Drawing.Point(77, 128);
            this.chkSlip.Name = "chkSlip";
            this.chkSlip.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkSlip.Size = new System.Drawing.Size(41, 17);
            this.chkSlip.TabIndex = 6;
            this.chkSlip.Text = "slip";
            this.chkSlip.UseVisualStyleBackColor = true;
            // 
            // chkSpeed
            // 
            this.chkSpeed.AutoSize = true;
            this.chkSpeed.Checked = true;
            this.chkSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSpeed.Location = new System.Drawing.Point(124, 128);
            this.chkSpeed.Name = "chkSpeed";
            this.chkSpeed.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkSpeed.Size = new System.Drawing.Size(55, 17);
            this.chkSpeed.TabIndex = 7;
            this.chkSpeed.Text = "speed";
            this.chkSpeed.UseVisualStyleBackColor = true;
            // 
            // chkAcceleration
            // 
            this.chkAcceleration.AutoSize = true;
            this.chkAcceleration.Location = new System.Drawing.Point(185, 128);
            this.chkAcceleration.Name = "chkAcceleration";
            this.chkAcceleration.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkAcceleration.Size = new System.Drawing.Size(84, 17);
            this.chkAcceleration.TabIndex = 8;
            this.chkAcceleration.Text = "acceleration";
            this.chkAcceleration.UseVisualStyleBackColor = true;
            // 
            // txtLogs
            // 
            this.txtLogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogs.Location = new System.Drawing.Point(385, 12);
            this.txtLogs.Multiline = true;
            this.txtLogs.Name = "txtLogs";
            this.txtLogs.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLogs.Size = new System.Drawing.Size(839, 93);
            this.txtLogs.TabIndex = 9;
            this.txtLogs.WordWrap = false;
            // 
            // Visualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 762);
            this.Controls.Add(this.txtLogs);
            this.Controls.Add(this.chkAcceleration);
            this.Controls.Add(this.chkSpeed);
            this.Controls.Add(this.chkSlip);
            this.Controls.Add(this.chkThrottle);
            this.Controls.Add(this.picTelemetry);
            this.Controls.Add(this.picLinear);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.pictureBox_Canvas);
            this.Name = "Visualizer";
            this.Text = "Visualizer";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Canvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLinear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTelemetry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBox_Canvas;
        public System.Windows.Forms.TextBox txtStatus;
        public System.Windows.Forms.PictureBox picLinear;
        public System.Windows.Forms.PictureBox picTelemetry;
        public System.Windows.Forms.CheckBox chkThrottle;
        public System.Windows.Forms.CheckBox chkSlip;
        public System.Windows.Forms.CheckBox chkSpeed;
        public System.Windows.Forms.CheckBox chkAcceleration;
        public System.Windows.Forms.TextBox txtLogs;
    }
}