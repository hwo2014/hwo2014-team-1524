﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;

namespace Visualizer.Forms
{
    public class Vector
    {
        public double X;
        public double Y;
        public Vector()
        {
            X = 0.0;
            Y = 0.0;
        }

        public Vector(Vector v)
        {
            this.X = v.X;
            this.Y = v.Y;
        }

        public Vector(Point start, Point end)
        {
            X = end.X - start.X;
            Y = end.Y - start.Y;
        }

        public Vector(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public static double ConvertToRadians(double angleInDegrees)
        {
            return (Math.PI / 180) * angleInDegrees;
        }

        public static double ConvertToDegrees(double angle)
        {
            return angle * (180 / Math.PI);
        }

        /* Takes one vector and an angle and calculates the vector that would be build if the given vector would be translated by the angle*/
        public static Vector RotateVector(Vector a, double angle)
        {
            Vector rotateVector = new Vector(a);
            rotateVector.Rotate(angle);
            return rotateVector;
        }

        public void Rotate(double angleInDegrees)
        {
            Matrix matrix = new Matrix();
            matrix.Rotate((float)angleInDegrees);
            
            // Applying the transform that results in a rotated point                                      
            PointF[] points = new PointF[] { new PointF((float)X, (float)Y), };
            matrix.TransformPoints(points);
            this.X = points[0].X;
            this.Y = points[0].Y;
        }

        public Vector Copy()
        {
            return new Vector(X, Y);
        }
        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }
        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }
        public static Vector operator *(Vector a, Vector b)
        {
            return new Vector(a.X * b.X, a.Y * b.Y);
        }
        public static Vector operator *(Vector a, double Multiplier)
        {
            return new Vector(a.X * Multiplier, a.Y * Multiplier);
        }
        public static Vector operator *(double Multiplier, Vector a)
        {
            return new Vector(a.X * Multiplier, a.Y * Multiplier);
        }
        public static Vector Multiply(Vector v, double Multiplier)
        {
            return (v * Multiplier);
        }
        public void Multiply(double Multiplier)
        {
            Vector tempVector = (this * Multiplier);
            X = tempVector.X;
            Y = tempVector.Y;
        }
        public static Vector operator /(Vector a, double Divisor)
        {
            return new Vector(a.X / Divisor, a.Y / Divisor);
        }
        public static Vector Divide(Vector v, double Divisor)
        {
            return (v / Divisor);
        }
        public void Divide(double Divisor)
        {
            Vector tempVector = (this * Divisor);
            X = tempVector.X;
            Y = tempVector.Y;
        }

        public Vector Invert()
        {
            return this * -1;
        }

        public double Dot(Vector a)
        {
            return X * a.X + Y * a.Y;
        }
        public static double Dot(Vector v, Vector w)
        {
            return (double)(v.X * w.X + v.Y * w.Y);
        }

        public Vector Cross(Vector v)
        {
            return new Vector(
              X * v.Y - Y * v.X,
              Y * v.X - X * v.Y);
        }
        public static Vector Cross(Vector v, Vector w)
        {
            return v.Cross(w);
        }

        public Vector NegativeOrtho()
        {
            return new Vector(-Y, X);
        }

        public Vector PositiveOrtho()
        {
            return new Vector(Y, -X);
        }

        public double Length()
        {
            return (double)(Math.Sqrt(X * X + Y * Y));
        }
        public void Normalize()
        {
            double length = this.Length();

            X /= length;
            Y /= length;
        }

        public void Normalize(double length)
        {
            Normalize();
            Multiply(length);
        }

        public static Vector Normalize(Vector v)
        {
            double length = v.Length();

            return new Vector(v.X / length, v.Y / length);
        }
        public void Reverse()
        {
            X = -X;
            Y = -Y;
        }

        public Point ToPoint()
        {
            return new Point(X, Y);
        }

        public Vector MakeUnitVectorInXYplane(double angle)
        {
            double X = Math.Cos(ConvertToRadians(angle));
            double Y = Math.Sin(ConvertToRadians(angle));
            return new Vector(X, Y);
        }

        public double GetXYVectorAngle()
        {
            double angle = Math.Asin(X);
            return ConvertToDegrees(angle);
        }
    }
}
