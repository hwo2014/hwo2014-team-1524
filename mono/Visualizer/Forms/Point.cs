﻿
namespace Visualizer.Forms
{
    public class Point
    {
        public double X;
        public double Y;
        public Point(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public Point(Vector v)
        {
            this.X = v.X;
            this.Y = v.Y;
        }
    }
}
