﻿namespace Visualizer.Forms
{
    using System.Windows.Input;

    using Point = System.Drawing.Point;

    public class PositionData
    {
        public Vector moveVector;
        public Forms.Point startPoint;
        public Forms.Point endPoint;
    }

    public class PieceWithPosition : Piece
    {
        public PositionData position;
        public PieceWithPosition(Piece piece)
        {
            this.angle = piece.angle;
            this.isSwitch = piece.isSwitch;
            this.length = piece.length;
            this.pieceId = piece.pieceId;
            this.radius = piece.radius;
            this.turboPiece = piece.turboPiece;
        }
    }
}
